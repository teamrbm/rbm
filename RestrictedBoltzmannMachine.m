% Restricted Boltzmann Machine
%
% Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% Coded inspired from:
%   - http://www.mathworks.com/matlabcentral/fileexchange/42853-deep-neural-network
%   - http://www.kyunghyuncho.me/home/code
%
% References
%  [1] Hinton, G. (2010)."A Practical Guide to Training Restricted Boltzmann Machines.
%      Computer, 9(3), 1. doi:10.1007/978-3-642-35289-8_32
%
% See also: AbstractMomentum, ConstantMomentum, PiecewiseMomentum,
% AbstractLearningRate, ConstantLearningRate, AdaptiveLearningRate
%
classdef RestrictedBoltzmannMachine < AbstractRestrictedBoltzmannMachine
    
    properties
        % Model Properties
        
        W = []; %Weight Matrix
        c = []; %visible layer bias
        b = []; %hidden layer bias
        
        
        %the following properties refers to debug level 2
        
        Ws = []; %Tracks how W evolves
        cs = []; %Tracks how the visible layer's biases evolve
        bs = []; %Tracks how the hidden layer's biases evolve
        
        ActualSparsity = NaN;
        
        %Stop training when NaNs or Infs are detected
        StopOnNansInfs = false;
        
        FullPerfomanceComputation = true; %computes performances on the entire dataset or minibatch at the time
    end
        
    properties(Access=protected,Hidden,Transient)
        CurrentFigure = [];
    end
       
    
    methods
        
        function this = RestrictedBoltzmannMachine(v,h)
            % Constructor
            % v: number of visible layers
            % h: number of hidden layers

            this = this@AbstractRestrictedBoltzmannMachine;
            
            if (nargin>0)
                this.initialise(v,h);
            end
        end
        
        
        function this=initialise(this,v,h,sigma)
            % Initialize Weights matrix + biases
            % sigma (opt): Weight matrix are initilised from a Normal
            %              distribution of 0 mean and variance sigma
            %              (Default: 0.01 [1])
            if (nargin<4)
                %Refer to [1] for further details
                sigma = 0.01;
            end
            
            this.c = zeros(1,v);
            this.b = zeros(1,h);
            this.W = randn(v,h) .* sigma;
            this.initialiseWithCurrentStatus;
        end
        
        
        function this=initialiseWithCurrentStatus(this)
            % Reset the status of the machine such that it can be trained
            % using the current status (usually is previous pre-training)
            
            this.Status ='initialised';
        end
        
        function this=resumeTrain(this)
            this.Status = 'training';
        end
                
        function this=Train(this,D)
            %Train the RBM
            % X: m-by-n matrix data of m observation of n features.
            %    N must be equal to the number of visible units.
            
            this.cudaCheck;
            
            if (ismatrix(D))
                D = Data.DefaultData(D);
            elseif (~isa(D,'Data.DefaultData'))
                error('Invalid data to train');
            end
            
            D.preprocess();
            
            this.postprocessedData(D);
            
            [nData, nFeatures] = size(D.X);
                        
            if (nFeatures ~= numel(this.c))
                error('Number of features and number of visible units mismatch.')
            end
            
            %Computer the total number of batches
            this.totBatches = this.getNumberOfMiniBatches(D);
            
            switch(this.Status)
                %starting a new training process
                case {'initialised', 'pretrained'}
                    if (this.Debug>1)
                        %no permutation if Debug level > 1
                        this.DataRandomVector = 1:nData;
                    else
                        this.DataRandomVector = randperm(nData);
                    end
                    
                    T = 1;
                    startBatch = 1;
                    
                    if (this.Debug>0)
                        [this.RMSE, this.Energies, this.ElapsedTime] = deal(zeros(this.LoggingLength,1));
                        
                        if (this.Debug>1 && ~this.Cuda)
                            this.initialiseTrackedVariables();
                        end
                    end
                    
                    %resuming previous training
                otherwise
                    T = this.Epoch;
                    startBatch = this.nBatch;
            end
            
            %changing status
            this.Status ='training';
            
            %if cuda, move data to gpu
            D = this.copyToDevice(D);
            
            while (~this.TrainingEventListener.StopCriterion(this,T))
                this.Epoch = T;
                if(this.Debug>0)
                    timer = tic;
                end
                
                %keep sparsity measurements
                this.Sparsity = zeros(this.totBatches,1);
                
                for i = startBatch:this.totBatches
                    %Update the number of batch being optimised
                    this.nBatch = i;
                    
                    this = this.runSubEpoch(D,T,i);
                    
                    if (this.checkForNansInfs(T,i) && this.StopOnNansInfs)
                        this.Status='interrupted';
                        return;
                    end
            
                    this.TrainingEventListener.MiniBatchResults(this,T,i,this.totBatches);
                end
                
               
                if (this.Debug>0)                       
                    Xr = this.computeEpochRMSE(D);
                                        
                    elapsedTime = toc(timer);
                    fe = 0;%this.freeEnergy(D);
                    rmse = gather(mean(sqrt(sum((D.X(:) - Xr.X(:)).^2,2))));
                    
                    if (this.Debug>1)
                        this.LastReconstructionData.X = D.data;
                        this.LastReconstructionData.Xr = Xr.data;
                    end
                    
                    clear Xr;
                    
                    this.appendTo('RMSE',rmse);
                    this.appendTo('Energies',mean(fe));
                    this.appendTo('ElapsedTime',elapsedTime);
                end
                
                this.TrainingEventListener.EpochResults(this,T);
                
                startBatch = 1;
                T = T+1;
                
                %Shuffle the dataset
                this.DataRandomVector = randperm(nData);
            end
            
            this.ActualSparsity = mean(this.Sparsity);
            
            this.gatherData;
            this.Status ='trained';
        end
        
        function this = runSubEpoch(this,D,T,i)
            %gibbs sampling
            v0 = this.getBatch(D,i);
            [v0_drawn,h0_prob,v1_drawn,h1_prob] = this.GibbsSampling(v0);

            %computing gradiends
            nabla = this.computeGradients(v0_drawn,h0_prob,v1_drawn,h1_prob);

            miniBatchSize = size(v0.data,1);

            momentum = this.Momentum.getValue(T);
            eta = this.Eta.getLearningRate(D,T,i,nabla,momentum,v0_drawn,h0_prob,v1_drawn,h1_prob);

            if (this.Cuda)
                momentum = gpuArray(momentum);
                learningRate = gpuArray(eta/miniBatchSize);
            else
                learningRate = eta/miniBatchSize;
            end


            nabla = this.applyMomentum(nabla,learningRate,momentum);
            %update network parameters
            this.updateTheta(nabla);

            %get Regularized Deltas
            [r_nabla] = this.RegularisationTerm.computeRegularisedGradients(v0,h0_prob,v1_drawn,h1_prob);
            this.updateTheta(r_nabla,false,true);
            
            
            this.Sparsity(i) = sum(gather(h0_prob.X(:)))     ./ (this.numberOfHiddenUnits*this.MiniBatchSize);
        end
                
        function [v0_drawn,h0_prob,v1_drawn,h1_prob] = GibbsSampling(this,v0)
            % Gibbs Sampling
            % Input:
            %  v0 : visible layer
            %
            % Output:
            %   v0_drawn: drawn visible layer
            %   h0_prob : probability that hidden units are active
            %   v1_drawn: reconstructed visible data
            %   h1_prob : hidden layer given the reconstructed data
            %
            %  See also: probHgivenV, probVgivenH, drawH, drawV
                        
            %% Debug
            if (this.Debug >= 3)
                persistent ReconstrFigure;
                
                if (isempty(ReconstrFigure) || ~ishandle(ReconstrFigure))
                    ReconstrFigure = figure;
                    subplot(1,2,1); title('Original Pattern');
                    subplot(1,2,2); title('Reconstructed Pattern');
                end
                
                set(0,'currentfigure',ReconstrFigure);
            end
            
            %% Step 1
            %check whether samples from the visible layer should be drawn
%             if (this.DrawSamples.V)
%                 v0_drawn = this.drawV(v0);
%             else
%                 v0_drawn = v0;
%             end

            v0_drawn = v0;
            %compute the hidden layer activetion
            h0_prob = this.probHgivenV(v0_drawn);
            %copy the status of the current hidden layer as the previous one to
            %prepare it for the next round of the Gibbs sampling
            
            if (this.Persistent && ~isempty(this.PersistentState))
                hn_1_prob = this.PersistentState;
            else
                hn_1_prob = h0_prob;
            end
            
            %% Further steps of Gibbs Sampling
            steps = this.NumberGibbsSamplingSteps;
            for i=1:steps
                %hn_1_prob.gibbs_step = i;
                % Draw samples from the hidden layer
                
                if (this.DrawSamples.H)
                    hn_1_drawn = this.drawH(hn_1_prob);
                else
                    hn_1_drawn = hn_1_prob;
                end
                %computer visible layer probabilities
                vn_prob  = this.probVgivenH(hn_1_drawn);
                
                %if we want to draw samples from the visible layer, then it will
                %do so
                if (this.DrawSamples.V)
                    vn_drawn = this.drawV(vn_prob);
                else
                    %otherwise it will keep working with the probabilities
                    vn_drawn = vn_prob;
                end
                %computing the probabilities of the hidden layer for the current
                %step of the sampling
                hn_prob  = this.probHgivenV(vn_drawn);
                %store result
                v1_drawn = vn_drawn;
                %update h_(n-1) to repeat another round of Gibbs Sampling (if
                %needed)
                [hn_1_prob,h1_prob] = deal(hn_prob);
            end
            
            %v1_drawn = rmfield(v1_drawn,'gibbs_step');
            %h1_prob  = rmfield(h1_prob,'gibbs_step');
            
             if (this.Persistent)
                 this.PersistentState = h1_prob;
             end
             
             %% Debug plot
             if (this.Debug >= 3)
                 if (~isempty(v0_drawn.Shape))
                     I  = reshape(v0_drawn.data(1,:),v0_drawn.Shape);
                     Ir = reshape(v1_drawn.data(1,:),v0_drawn.Shape);
                     
                     subplot(1,2,1); imshow( I,[] );
                     subplot(1,2,2); imshow( Ir,[] );
                     
                     
                     %draw now with a small probability.
                     %saving the state will make the algorithm consistent
                     %when it is run with or without Debug.
                     state = rng;
                     r = rand;
                     
                     if (r>0.95)
                         drawnow;
                     end
                     
                     rng(state);
                 end
             end
        end
                
        
        function H = probHgivenV(this,V,Weights)
            %Computes the p(h|v) probabilities
            % V: visibile layer
            % W: weight matrix (default this.W)
            %
            % Output
            %   p: activation probabilities
            
            if (nargin<3)
                Weights = this.W;
            end
            
            p = RestrictedBoltzmannMachine.sigmoid( bsxfun(@plus,V.data * Weights,this.b));
            
            %copy the structure
            H = V.copy();
            H.X = p;
        end
        
        function V = probVgivenH(this,H,Weights)
            %Computes the p(v|h) probabilities
            % h = invisible layer
            % W: weight matrix (default this.W)
            %
            % Output
            %   p: activation probabilities
            
            if (nargin<3)
                Weights = this.W;
            end
            
            p = RestrictedBoltzmannMachine.sigmoid( bsxfun(@plus, H.data * Weights',this.c));
            
            %copy the structure
            V = H.copy();
            V.X = p;
        end
        
        
        function H = drawH(~,probabilities)
            %Draw samples H from a binomial distribution, using the provided
            %probabilties
            %   probabilities: input probabilities
            % Output
            %   H: drawn H's
            
            %copy the structure
            H = probabilities.copy();
            
            H.X = binornd(1,probabilities.data);
        end
        
        
        function V = drawV(~,probabilities)
            %Draw samples V from a binomial distribution, using the provided
            %probabilties
            %   probabilities: input probabilities
            % Output
            %   V: drawn V's
            
            %in BBRBM visible and hidden layer shares the same drawing algorithm
            
            V = probabilities.copy();
            V.X = binornd(1,probabilities.data);
        end
        
                
        

        
        function this = showWeight(this,shapeVisible,shapePlot,parent,T)
            %Show learned weights as images
            %Input:
            %  shapeVisible: [row, column] for the patches in the visible layer
            %  shapePlot   : number of [rows,columns] of the tiled output
            %                images. It is not required to have the same number 
            %                of the hidden units.
            %  parent      : (optional) parent axis where the output is showed.
            %  T           : (optional) slice of the W matrix to show (only for
            %                tensor methods)
            
            %checking input arguments
            if (nargin<4 || isempty(parent))
                parent = gca;
            end
            
            if (nargin<5)
                T=1;
            end
            
            %reshape weight such that visible units have the original shape
            stack = reshape(this.W(:,:,T),shapeVisible(1), shapeVisible(2),[]);
            %get size of the actual patches
            [height,width,~] = size(stack);
            %create output image
            img = zeros( height*(shapePlot(1)) +1, width*(shapePlot(2)) +1 );
            
            %counter for the patches
            k=1;
            %row position
            posR = 2;
            for i = 1:shapePlot(1)
                %column position
                posC = 2;
                for j = 1:shapePlot(2)
                    %copying the patch
                    try
                        img(posR:posR+height-1,posC:posC+width-1) = stack(:,:,k);
                    catch E
                        rethrow(E)
                    end

                    k=k+1;
                    posC = posC + width;
                end
                
                posR = posR + height;
            end
            
            imshow(img,[],'Parent',parent);
        end
        
        function n = numberOfHiddenUnits(this)
            %Returns the number of hidden units
            
            n = numel(this.b);
        end
        
        function n = numberOfVisibleUnits(this)
            %Returns the number of visible units
            
            n = numel(this.c);
        end
        
        function H = extractFeatures(this,X)
            %Extract features from a trained RBM
            %
            %Input
            %   X: input data where features are extracted from
            %
            %Output
            %   H: extracted features
            %
            
            if (~X.Preprocessed)
                X.preprocess;
            end
            
            H = this.probHgivenV(X,this.W);
            
            H = H.data;
        end
        
        function nabla = applyMomentum(this,nabla,lr,momentum)
            %Apply momentum to the computed gradients
            %
            %Input:
            %   nabla    : current gradients
            %   lr       : learning rate
            %   momentum : momentum to apply to previous gradients
            %
            % Output:
            %   nabla : gradients with momentum terms
            %
            
            fname = fieldnames(nabla);

            for t = 1:numel(fname)
                term = 0;
                if (isfield(this.LastGradients,fname{t}))
                    term = this.LastGradients.(fname{t});
                end
                
                
                nabla.(fname{t}) = momentum * term + lr * nabla.(fname{t});
            end
        end
        
        %Apply gradients to the set of parameters of the machine, without
        %actually alter the status of the machien
        %
        % Input:
        %   nabla: structure containing the gradients (e.g. W,b,c)
        %
        % Output:
        %   Theta: set of parmeters updated using nabla
        function varargout = applyGradients(this,nabla)
            varargout{:} = this.updateTheta(nabla,true);
        end
        
        function D = copyToDevice(this,D)
            %This function performs operation before the RBM starts to be
            %trained
            %
            %Input
            % D: struct of data to be trained (data field mandatory)
            %
            %Output
            % Dgpu: data moved into the GPU
            
            if (this.Cuda)
                this.W = gpuArray(this.W);
                this.b = gpuArray(this.b);
                this.c = gpuArray(this.c);
                
                D.X = double(D.X);
                
            end
        end
        
        function this = gatherData(this)
            if (this.Cuda)
                this.W = gather(this.W);
                this.b = gather(this.b);
                this.c = gather(this.c);
                
                fname = fieldnames(this.LastGradients);
                
                for t = 1:numel(fname)
                    this.LastGradients.(fname{t}) = gather(this.LastGradients.(fname{t}));
                end
            end
        end
        
    end
    
    methods(Access=protected)
        function Xr = computeEpochRMSE(this,D)
            if (this.FullPerfomanceComputation)
                H0 = this.probHgivenV(D);
                Xr = this.probVgivenH(H0);
            else
                Xr = Data.DefaultData;
                M = size(D);
                batches = 1:this.MiniBatchSize:M;
                batches(end+1) = M+1;
                
                for j=1:numel(batches)-1
                    chunk = batches(j):batches(j+1)-1;
                    H0 = this.probHgivenV(D(chunk,:));
                    X_tmp = this.probVgivenH(H0);
                    Xr(chunk,:) = gather(X_tmp.data);
                end
                
                clear X_tmp;
                clear H0;
            end
        end
        
        function data = dataToDump(this)
            data = dataToDump@AbstractRestrictedBoltzmannMachine(this);
            
            keys   = {'W','b','c','ActualSparsity'}';
            values = MiscFunctions.readProperties(this,keys);
            
            data = cat(1,data,cat(2,keys,values));
        end
        
        function s = checkForNansInfs(this,T,i)
           params = {'W','b','c'};
           
           s = false;
           
           for j=1:numel(params)
               if (any(isnan(this.(params{j})(:))) ||  any(isinf(this.(params{j})(:))))
                   if (this.Debug>1)
                       error('NAN detected @ Epoch %d Batch %d',T,i);
                   else
                       s=true;
                   end
               end
           end
        end
        
        function nabla = computeGradients(~,v0_drawn,h0_prob,v1_drawn,h1_prob)
            % Computes gradient of the CD algorirthm
            % Input:
            %  see output Gibbs Sampling
            %
            % Output:
            %   nabla: Computed gradients
            
            nabla.W = (v0_drawn.data' * h0_prob.data) - (v1_drawn.data'*h1_prob.data);
            nabla.b = sum(h0_prob.data - h1_prob.data,1);
            nabla.c = sum(v0_drawn.data - v1_drawn.data,1);
        end

         % postprocesseData
         %
         % This function is called after input training data are preprocessed
         %
         % Input:
         %   D: preprocessed data
         %
         % SEE ALSO: DataPreprocessing.EmptyPreprocessor, DataPreprocessing.Normalisation01
         function this = postprocessedData(this, D)
         end
        
        function [varargout] = updateTheta(this,nabla,fakeUpdate,addLastGradients)
            %Update model parameters theta = {W,b,c}
            %This method also update the tracking for these
            %parameters, if the debug level is at least 2.
            %
            %Input:
            %   nabla      : gradients
            %   fakeUpdate : do not actually mutate the status of the machine
            %
            
            type = 'double';
            if (this.Cuda)
                type = 'gpuArray';
            end
            
            if (nargin<3)
                fakeUpdate=false;
            end
            
            if (nargin<4)
                addLastGradients=false;
            end
            
            if (~isfield(nabla,'W')), nabla.W = zeros(size(this.W),type); end
            if (~isfield(nabla,'b')), nabla.b = zeros(size(this.b),type); end
            if (~isfield(nabla,'c')), nabla.c = zeros(size(this.c),type); end
             
            newW = this.W + nabla.W - this.WeightCost * this.W;
            newB = this.b + nabla.b;
            newC = this.c + nabla.c;
            
            if (~fakeUpdate)
                %Thetas are updated in a new variable in order to force variable
                %replication, which avoids passage by reference to have atomic
                %updates. Also update the last gradients for the momentum at the
                %same way
                [this.W,this.b,this.c] = deal(newW,newB,newC);
                
                if (addLastGradients)
                    fields = fieldnames(nabla);
                    
                    for i = 1:numel(fields)
                        this.LastGradients.(fields{i}) = this.LastGradients.(fields{i}) ...
                                                         + nabla.(fields{i});
                    end
                else
                    this.LastGradients = nabla;
                end
                
                varargout{1} = this;

                if ((this.Debug > 2) && ~this.Cuda)
                    this.appendTo('Ws',this.W);
                    this.appendTo('bs',this.b);
                    this.appendTo('cs',this.c);
                end
            else
                varargout{1} = struct('W',newW,'c',newC,'b',newB);
            end
        end
                
        function this = initialiseTrackedVariables(this)
            %Initiliase the object properties used to track how model
            %parameters evolve
            
            this.bs = zeros(this.LoggingLength,numel(this.b));
            this.cs = zeros(this.LoggingLength,numel(this.c));
            this.Ws = zeros([size(this.W),this.LoggingLength]);
        end
    end
    
    methods(Static)
        function E = freeEnergy(varargin)
            %Computes the free energies from the provided visible data
            % Input (in the following order)
            % 1 - Data
            % 2 - Weight Matrix
            % 3 - Visible layer bias (row vector)
            % 4 - Hidden layer bias (row vector)
            
            % Output
            %   E : free energy computed for each sample X
            
            if ((nargin>1) && isa(varargin{2},'RestrictedBoltzmannMachine'))
                [X,r] = deal(varargin{:});
                E = RestrictedBoltzmannMachine.freeEnergy(X,r.W,r.c,r.b);
            else
                [X,W,c,b] = deal(varargin{:});
                
                if (isa(X,'Data.DefaultData'))
                    X = X.data;
                end

                nData = size(X,1);

                visible_term = X * c';
                hidden_term = sum(log(1+exp(repmat(b,nData,1)+X*W)),2);

                E = gather(-visible_term - hidden_term);
            end
        end
    end
end
