% AbstractRegularisation
%
% This class allows to compute regularised gradients from RBM
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or  any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

classdef AbstractRegularisation < matlab.mixin.SetGet & matlab.mixin.Copyable
    
    properties
        RBM = [];
    end
    
    methods
         function this = AbstractRegularisation(rbm)
            if nargin>0
                this.RBM = rbm;
            end
         end
    end
    
    methods(Access=protected)
        function t = isCuda(varargin)
            this = varargin{1};
            varargin = varargin(2:end);
            
            t = false;
            
            if (~isempty(this.RBM))
                t = this.RBM.Cuda;
            else
                for i = 1:numel(varargin)
                    if (isa(varargin{i},'gpuArray'))
                        t=true;
                    end
                end
            end
        end
    end
    
    methods(Abstract)
        %Computes regularised gradients from original data (v0,h0) and
        %reconstructed ones (v1,h1)
        %
        % v0: minibatch data
        % h0: corresponding hidden units
        % v1: reconstructed data
        % h1: corresponding hidden units from reconstructed data
        %
        % Output:
        %   r_nabla: regularised gradients
        %
                
        [r_nabla] = computeRegularisedGradients(this,v0,h0,v1,h1);
    end
end

