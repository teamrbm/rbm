% V2SparseRegularisationTerm
%
% This class computes the Sparsity Regularisation term as described in [*]
% 
% [*] Lee, H., Ekanadham, C., & Ng, A. Y. (2008). Sparse deep belief net 
%     model for visual area V2. Advances in Neural Information Processing 
%     Systems 20 (NIPS 2007), 873?880. doi:10.1.1.120.9887
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or  any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%

classdef V2SparseRegularisationTerm < Regularisation.AbstractRegularisation
    
    
    properties
        Lambda = 0.05; %Regularisation Constant
        SparsityTarget = 0.01; %Number of units that we expect activated in the hidden layer
        updateWeightMatrix = false; %Read [*] for further details
    end
    
    methods
        function this = V2SparseRegularisationTerm(l,p)
            % Class Constructor
            %
            % l: regularisation constant (default 1)
            % p: sparsity target (default 1)
            
            if (nargin>1)
                this.SparsityTarget = p;
            end
            
            if (nargin>0)
                this.Lambda = l;
            end
        end
        
        function [nabla] = computeRegularisedGradients(this,v0,h0,~,~)
            [nData,nVUnits] = size(v0.data);
            
            sparsityTerm = mean(this.SparsityTarget - h0.data);
            hidUnitsTerm = h0.data.*(1-h0.data);
            
            cuda = false;
            if(this.isCuda(v0.data))
                l = gpuArray(this.Lambda);
                cuda = true;
            else
                l = this.Lambda;
            end
            
            nabla.b = l * sparsityTerm .* mean(hidUnitsTerm);
            
            if (this.updateWeightMatrix)
                posNegTerm = v0.data'*(hidUnitsTerm)/nData;
                nabla.W = l * repmat(sparsityTerm,nVUnits,1).*posNegTerm;
            else
                nabla.W = zeros(size(v0.data,2),size(h0.data,2));
                if (cuda)
                    nabla.W = gpuArray(nabla.W);
                end
            end
            
            if (~isempty(this.RBM))
                nabla.W = repmat(nabla.W,1,1,size(this.RBM.W,3));
            end
        end
    end
    
end

