% KMeansInitialiser
%
% Initialise sigma using any kmeans implementtion defined by subclasses
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or  any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

classdef (Abstract) KMeansInitialiser < SigmaInitialiser.ExternalSigmaInitialiser

	properties
		Clusters = 100 %Number of clusters kmeans has to find
	end

    methods
         function this = KMeansInitialiser(varargin)
            this = this@SigmaInitialiser.ExternalSigmaInitialiser(varargin{:});
         end

         function z = initialise(this,X)
         	[labels,centres] = this.loadKMeans(X);

         	sigma = mean( (X - centres(labels,:)).^2);

         	if (this.SingleSigma)
                sigma = mean(sigma) * ones(size(sigma));
            end
            
            sigma = sqrt(sigma);

         	z = log(sigma);
         end

         [labels,centres] = loadKMeans(this,X);
    end
end

