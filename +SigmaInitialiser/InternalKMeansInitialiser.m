% InternalKMeansInitialiser
%
% Initialise sigma using the internal matlab initialiser
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or  any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% SEE ALSO: kmeans

classdef InternalKMeansInitialiser < SigmaInitialiser.KMeansInitialiser

    methods
        function this = InternalKMeansInitialiser(varargin)
            this = this@SigmaInitialiser.KMeansInitialiser(varargin{:});
            this.Parameters.Start = 'plus'; %use kmeans++ as centroid initialiser
         end

         function [labels,centres] = loadKMeans(this,X)
            if (this.Verbose)
                this.Parameters.Display = 'iter';
            end

            keys   = fieldnames(this.Parameters);
            params = struct2cell(this.Parameters);
            
            p = [ keys(:)'; params(:)' ];
           
            [labels,centres] = kmeans(X,this.Clusters,p{:});
         end
    end
end

