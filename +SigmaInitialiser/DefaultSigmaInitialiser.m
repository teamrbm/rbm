% DefaultSigmaInitialiser
%
% Initialise sigma is a GBRBM with ones
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or  any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

classdef DefaultSigmaInitialiser < SigmaInitialiser.AbstractSigmaInitialiser
    
    methods
         function this = DefaultSigmaInitialiser(varargin)
            this = this@SigmaInitialiser.AbstractSigmaInitialiser(varargin{:});
         end
    
        function z = initialise(~,D)
            z = zeros(1,size(D,2));
        end
    end
end

