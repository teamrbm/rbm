% LiteKMeansInitialiser
%
% Initialise sigma using the internal matlab initialiser
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or  any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% SEE ALSO: kmeans

classdef LiteKMeansInitialiser < SigmaInitialiser.KMeansInitialiser

    methods
        function this = LiteKMeansInitialiser(varargin)
            this = this@SigmaInitialiser.KMeansInitialiser(varargin{:});
            this.Parameters.MaxIterations = 100;
         end

         function [labels,centres] = loadKMeans(this,X)
            [labels,centres] = MiscFunctions.litekmeans(gather(X),this.Clusters,this.Verbose,this.Parameters.MaxIterations);
         end

    end
end

