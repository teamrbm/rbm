% AbstractSigmaInitialiser
%
% This class defines an interface how the sigma in GBRM should be initialised
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or  any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

classdef (Abstract) AbstractSigmaInitialiser < matlab.mixin.SetGet & matlab.mixin.Copyable
    
    properties
        SingleSigma = true;
        Verbose = 0 ;
    end
    
    methods
         function this = AbstractSigmaInitialiser(varargin)
            if nargin>0
                this.SingleSigma = varargin{1};
            end
         end
    end
    
    
    methods(Abstract)
        %Initialise sigma using the provided input data
        %
        % X: input (processed) data
        %
        % Output:
        %   z: log of the sigma
        %
                
        z = initialise(this,X);
    end
end

