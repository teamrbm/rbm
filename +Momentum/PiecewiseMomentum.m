% Piecewise Momentum
%
% Momentum is constant til a certain time marker, then it will change to another
% value
%
% Example:
%  momentum = PiecewiseMomentum([0.5 0.9], [1 5] )
%
%  In this case, between Epoch 1 and 4 the momentum is 0.5, then it will become
%  0.9.
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or  any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%

classdef PiecewiseMomentum < Momentum.AbstractMomentum

    methods
        function this = PiecewiseMomentum(values,ts)
            if (isvector(values) && (numel(values)==numel(ts)))
                this.Values = values;
                this.TimeMarkers = ts;
            else
                error('Invalid argument: ''value'' must be scalar');
            end
        end
        
        function v = getValue(this,t)
            v = 0;
            
            %check which momentum is the correct one
            idx = find(this.TimeMarkers <= t,1,'last');
            
            if (~isempty(idx))
                v = this.Values(idx);
            end
        end
    end
    
end

