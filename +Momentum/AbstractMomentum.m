% AbstractMomentum
%
% This class allows to have a time-changing momentum value.
% This is a superclass that is kept as generic as possible to be open to all
% the possibilities.
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or  any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%

classdef AbstractMomentum < matlab.mixin.SetGet & matlab.mixin.Copyable
    properties(Access=public)
        Values = 0.5; %Minimum value the momentum can have
        TimeMarkers = []; %Number of iteration when a change on the momentum should occur
    end
    
    methods 
        function this = set.Values(this,val)
            this.checkValues(val);
            this.Values = val;
        end
    end
    
    methods(Abstract)
        %Returns the current value for the momentum at time t
        v = getValue(t);
    end
    
    methods(Access=protected)
        function checkValues(~,v)
            if (any(v<0) || any(v>1))
                error('Invalid value for the momentum');
            end
        end
    end
        
end

