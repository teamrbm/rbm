% ConstantMomentum
%
%  This class define a constant-time momentum
%
%   Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%

classdef ConstantMomentum < Momentum.AbstractMomentum

    methods
        function this = ConstantMomentum(value)
            if (nargin==0)
                value = .9;
            end
            
            if (isscalar(value))
                this.Values = value;
            else
                error('Invalid argument: ''value'' must be scalar');
            end
        end
        
        function v = getValue(this,~)
            v = this.Values(1);
        end
    end
    
end

