%  Normalisation01
%
%  Normalise data into the interval [0,1]
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef Normalisation01 < DataPreprocessing.EmptyPreprocessor
    
    properties
        Bounds = struct('Min',NaN,'Max',NaN);
    end
   
    methods
        
        function this=Normalisation01()
        end
        

        function X = preprocessData(this,X)
            if (isnan(this.Bounds.Min) || isnan(this.Bounds.Max))
                this.computePreprocessingParameters(X)
            end
            
            int = this.Bounds.Max - this.Bounds.Min;
            
            if (this.memorySafe)
                for i=1:numel(X)
                    X(i) = (X(i) - this.Bounds.Min)/int;
                end
            else
                X = (X - this.Bounds.Min)./int;
            end
        end
        
    end
    
    methods (Access=protected)      
        function computePreprocessingParameters(this,X)
            % performing min of min (max of max) may safe memory
            this.Bounds.Min = min(min(X));
            this.Bounds.Max = max(max(X));
        end
    end
    
end

