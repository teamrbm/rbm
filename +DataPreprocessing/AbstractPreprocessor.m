%  AbstractPreprocessor
%
%   This class defines an abstract data preprocessor for RBM
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef (Abstract) AbstractPreprocessor < handle
    
    properties
        memorySafe = 0;
    end
    
    methods
        
        function this=AbstractPreprocessor()
        end
        
        % Preprocess input data
        % Input:
        %   X: data to be preprocessed
        %
        % Output:
        %   X: preprocessed data
        %
        X = preprocessData(this,X);
    end
    
    methods (Access=protected)
        % Computes data for the preprocessing. Useful to compute once
        % in order to use them for the testing set
        %
        % Input:
        %   X: data to be preprocessed
        %
        
        computePreprocessingParameters(this,X);
    end
    
end

