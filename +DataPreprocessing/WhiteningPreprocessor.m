%  ZscorePreprocessor
%
%  Whitening the data using Zero-Phase-Component Analysis (ZCA)
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef WhiteningPreprocessor < DataPreprocessing.AbstractPreprocessor
    
    properties
        epsilon = 1e-6;
    end
    
    properties (Access=public)
        mu = []; %data mean
        M = [];  %projection matrix
    end
    
    methods
        function this = WhiteningPreprocessor(varargin)
            if (nargin>0)
                this.epsilon = varargin{1};
            end
        end
        
        function X = preprocessData(this,X)
            if (isempty(this.mu)||isempty(this.M))
                this.computePreprocessingParameters(X);
            end
            
            X = bsxfun(@minus,X,this.mu) * this.M;
        end
    end
    
    methods(Access=protected)
        function computePreprocessingParameters(this,X)
            this.mu = mean(X);
            
            X = bsxfun(@minus,X,this.mu);
            G = cov(X);
            
            [U,S] = eig(G);
            
            D = diag(sqrt(1./(diag(S)+this.epsilon)));
            
            this.M = U * D * U';
        end
    end
    
    methods(Static)
        function S = covariance(X,memorySafe)
            if (nargin<2)
                memorySafe=false;
            end
            
            if (memorySafe)
                %% Todo
                error('Not implemented yet')
            else
                S = cov(X);
            end
        end
    end
end

