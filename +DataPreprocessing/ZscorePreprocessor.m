%  ZscorePreprocessor
%
%  Normalise the input data by subtracting the mean and dividing them by the standard deviation
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef ZscorePreprocessor < DataPreprocessing.AbstractPreprocessor
        
    properties (SetAccess=protected,GetAccess=public)
        mu = [];    %data mean
        sigma = []; %data standard deviation
    end
    
    methods
        function X = preprocessData(this,X)
            if (isempty(this.mu))
                this.mu = this.computeMu(X);
            end
            
            if (~this.memorySafe)
                X = bsxfun(@minus,X,this.mu);
                
                if (isempty(this.sigma))
                    this.sigma = this.computeSigma(X);
                end
                
                X = bsxfun(@rdivide,X,this.sigma);
            else
                for t = 1:size(X,1)
                    X(t,:) = (X(t,:) - this.mu);
                end
                
                if (isempty(this.sigma))
                    this.sigma = this.computeSigma(X);
                end
                
                for t = 1:size(X,1)
                    X(t,:) = X(t,:)./this.sigma;
                end
            end
        end
    end
    
    methods(Access=protected)
        function mu = computeMu(~,X)
            mu = mean(X);
        end
        
        function sigma = computeSigma(this,X)
            if (this.memorySafe)
                [M, N] = size(X);
                
                mu = this.computeMu(X);
                sigma = zeros(1,N);
                
                for d=1:N
                    sigma(d) = sqrt(sum((X(:,d) - mu(d)).^2)./(M-1));
                end
                
            else
                sigma = std(X);
            end
            
            sigma(this.sigma==0)=1;
        end
    end
    
end

