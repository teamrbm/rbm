%  ICA Preprocessor
%
%  Computes the Independent Component Analysis on the inpit data
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef ICAPreprocessor < DataPreprocessing.AbstractPreprocessor & matlab.mixin.SetGet
    
    properties
        reductionFactor = 1
    end
    
    properties (Access=protected)
        mu = []; %data mean
        T  = []; %projection matrix
        A  = []; %projection matrix
    end
    
    methods
        function this = WhiteningPreprocessor(varargin)
            if (nargin>0)
                this.reductionFactor = varargin{1};
            end
        end
        
        function this = set.reductionFactor(this,r)
            if ((r>0) && (r<=1))
                this.reductionFactor = r;
            else
                error('Reduction factor has to be between 0 and 1.');
            end
        end
        
        function X = preprocessData(this,X)
            [n, d] = size(X);
            if (isempty(this.mu)||isempty(this.T) || isempty(this.A))
                r = round(d * this.reductionFactor);
                
                [X,this.A, this.T, this.mu] = MiscFunctions.PcaIca.myICA(X',r);
            else
                X =  (pinv(this.A)\this.T)*(X' - repmat(this.mu,1,n));
            end
            
            X = X';
        end
    end
    
    methods(Access=protected)
        function computePreprocessingParameters(this,X)
            this.mu = mean(X);
            sigma   = cov(X);
            
            [V,D] = eig(sigma);
            
            D = diag(sqrt(1/(diag(D)+this.epsilon)));
            
            
            this.M = V * D * V';
        end
    end
    
end

