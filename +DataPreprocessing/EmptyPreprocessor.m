%  EmptyPreprocessor
%
%  This class performs no data preprocessing
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef EmptyPreprocessor < DataPreprocessing.AbstractPreprocessor
    methods
        function X = preprocessData(~,X)
        end
    end
    
    methods(Access=protected)
        function computePreprocessingParameters(varargin)
        end
    end
    
end

