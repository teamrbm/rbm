T = Test.TestUnit('Expl Rot Inv RBM',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
T.Test;

T = Test.TestUnit('Expl Rot Inv RBM + Normalisation [0,1]',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('TrainingEventListener.MaxIterations',10);
D = Data.DefaultData(rand(1000,100),DataPreprocessing.Normalisation01);
T.Verbose=1;
T.Test(D);

T = Test.TestUnit('Expl Rot Inv RBM + ICA',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('TrainingEventListener.MaxIterations',10);
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ICAPreprocessor);
T.Verbose=1;
T.Test(D);

T = Test.TestUnit('Expl Rot Inv RBM + Whitening',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('TrainingEventListener.MaxIterations',10);
D = Data.DefaultData(rand(1000,100),DataPreprocessing.WhiteningPreprocessor);
T.Verbose=1;
T.Test(D);

T = Test.TestUnit('Expl Rot Inv RBM + ZScore',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('TrainingEventListener.MaxIterations',10);
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
T.Verbose=1;
T.Test(D);

T = Test.TestUnit('Expl Rot Inv RBM + Adaptive Learning Rate',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D.Shape = [10,10];
T.Test(D);

T = Test.TestUnit('Expl Rot Inv RBM + Adaptive Learning Rate (Debug 1)',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Debug',1);
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D.Shape = [10,10];
T.Test(D);

T = Test.TestUnit('Expl Rot Inv RBM + Adaptive Learning Rate (Debug 2)',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Debug',2);
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D.Shape = [10,10];
T.Test(D);

T = Test.TestUnit('Expl Rot Inv RBM + Adaptive Learning Rate (Debug 3)',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Debug',3);
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D.Shape = [10,10];
T.Test(D);

T = Test.TestUnit('Expl Rot Inv RBM + ALR + PiecewiseMomentum',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('TrainingEventListener.MaxIterations',10);
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Momentum',Momentum.PiecewiseMomentum([0.5, 0.9],[1,10]));
T.Verbose=1;
T.Test;

T = Test.TestUnit('Expl Rot Inv RBM + Sparse Regularisation',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('TrainingEventListener.MaxIterations',10);
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.set('RegularisationTerm.RBM',T.machine);
T.Verbose=1;
T.Test;

T = Test.TestUnit('Expl Rot Inv RBM + Sparse Regularisation + ALR',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.set('TrainingEventListener.MaxIterations',10);
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.set('RegularisationTerm.RBM',T.machine);
T.Verbose=1;
T.Test;

%%
rng(1);
T = Test.TestUnit('ERI-RBM vs RBM + Sparse Regularisation + ALR',ExplicitRotInvRestrictedBoltzmannMachine(10,10,10));
T.machine.NumberOfBins=1;

D = Data.DominantOrientationData(rand(1000,100));
D.Bins=ones(size(D.X,1),1);
D.Shape=[10 10];

rng(1);
rbm = RestrictedBoltzmannMachine(100,10);
rbm.TrainingEventListener.MaxIterations=10;
rbm.Eta = LearningRate.AdaptiveLearningRate(rbm);
rbm.RegularisationTerm = Regularisation.V2SparseRegularisationTerm;
rbm.RegularisationTerm.RBM = rbm;
rbm.DrawSamples.V=false;
rbm.DrawSamples.H=false;
rbm.Debug=2;

assert(all(T.machine.W(:) == rbm.W(:)));

rbm.Train(D);

T.set('TrainingEventListener.MaxIterations',10);
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.set('DrawSamples.V',false);
T.set('DrawSamples.H',false);
T.set('Debug',2);
T.Verbose=1;

T.Asserts{1} = @(r)(all( (r.W(:) - rbm.W(:))<1e-6 ));

T.Test(D);


T = Test.TestUnit('ERI-RBM + Dump Training Event',ExplicitRotInvRestrictedBoltzmannMachine(4,4,2));
T.Verbose=1;
D = Data.DefaultData(rand(1000,16));
D.Shape=[4 4];

T.set('TrainingEventListener.MaxIterations',10);
T.set('NumberOfBins',4);
T.set('TrainingEventListener',Events.DumpTrainingEvent(D));
T.set('TrainingEventListener.Verbose',true);
T.set('TrainingEventListener.W.Slices',[1 3]);
T.set('TrainingEventListener.DataShape',D.Shape);
T.set('Debug',2);
T.set('TrainingEventListener.ProbeAt',2);
T.Test(D);
%remove image data
!rm Data.*.png
!rm W_*.png

T = Test.TestUnit('ERI-RBM + LibSVM Training Event',ExplicitRotInvRestrictedBoltzmannMachine(4,4,2));
T.Verbose=1;
T.machine.NumberOfBins = 4;

D_train = Data.DominantOrientationData(rand(1000,16));
D_train.Shape=[4 4];
D_train.computeDominantOrientation(T.machine.AngleBins,'onehot');
Y_train = round(rand(1000,1));

D_val = Data.DominantOrientationData(rand(100,16));
D_val.Shape=[4 4];
D_val.computeDominantOrientation(T.machine.AngleBins,'onehot');
Y_val = round(rand(100,1));

T.set('TrainingEventListener.MaxIterations',10);
T.set('TrainingEventListener',Events.LibSVMProbeTrainingEvent(D_train,Y_train,D_val,Y_val));
T.set('TrainingEventListener.Verbose',true);
T.set('Debug',2);
T.set('TrainingEventListener.ProbeAt',2);
T.set('TrainingEventListener.Settings','-s 0 -t 2');
T.Test(D_train);