T = Test.TestUnit('Vanilla RBM',RestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.Verbose=1;
T.Test;

T = Test.TestUnit('Vanilla RBM + Normalisation [0,1]',RestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
D_train = Data.DefaultData(rand(1000,10),DataPreprocessing.Normalisation01);
T.Verbose=1;
T.Test(D_train);

T = Test.TestUnit('Vanilla RBM + ICA',RestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
D_train = Data.DefaultData(rand(1000,10),DataPreprocessing.ICAPreprocessor);
T.Verbose=1;
T.Test(D_train);

T = Test.TestUnit('Vanilla RBM + Whitening',RestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
D_train = Data.DefaultData(rand(1000,10),DataPreprocessing.WhiteningPreprocessor);
T.Verbose=1;
T.Test(D_train);

T = Test.TestUnit('Vanilla RBM + ZScore',RestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
D_train = Data.DefaultData(rand(1000,10),DataPreprocessing.ZscorePreprocessor);
T.Verbose=1;
T.Test(D_train);

T = Test.TestUnit('Vanilla RBM + Adaptive Learning Rate',RestrictedBoltzmannMachine(100,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D_train = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D_train.Shape = [10,10];
T.Test(D_train);

T = Test.TestUnit('Vanilla RBM + Adaptive Learning Rate (Debug 1)',RestrictedBoltzmannMachine(100,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Debug',1);
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D_train = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D_train.Shape = [10,10];
T.Test(D_train);

T = Test.TestUnit('Vanilla RBM + Adaptive Learning Rate (Debug 2)',RestrictedBoltzmannMachine(100,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Debug',2);
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D_train = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D_train.Shape = [10,10];
T.Test(D_train);

T = Test.TestUnit('Vanilla RBM + Adaptive Learning Rate (Debug 3)',RestrictedBoltzmannMachine(100,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Debug',3);
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D_train = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D_train.Shape = [10,10];
T.Test(D_train);

T = Test.TestUnit('Vanilla RBM + ALR + PiecewiseMomentum',RestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Momentum',Momentum.PiecewiseMomentum([0.5, 0.9],[1,10]));
T.Verbose=1;
T.Test;

T = Test.TestUnit('Vanilla RBM + Sparse Regularisation',RestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.Verbose=1;
T.Test;

T = Test.TestUnit('Vanilla RBM + Sparse Regularisation + ALR',RestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.Verbose=1;
T.Test;

T = Test.TestUnit('Vanilla RBM + Default Training Event',RestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.Verbose=1;
T.set('TrainingEventListener.Verbose',true);
T.set('Debug',2);
T.Test;

T = Test.TestUnit('Vanilla RBM + Dump Training Event',RestrictedBoltzmannMachine(16,2));
T.Verbose=1;
D_train = Data.DefaultData(rand(1000,16));
D_train.Shape = [4 4];
T.set('TrainingEventListener.MaxIterations',10);
T.set('TrainingEventListener',Events.DumpTrainingEvent(D_train));
T.set('TrainingEventListener.Verbose',true);
T.set('TrainingEventListener.DataShape',D_train.Shape);
T.set('Debug',2);
T.set('TrainingEventListener.ProbeAt',2);
T.Test(D_train);
%remove image data
!rm Data.*.png
!rm W_*.png

T = Test.TestUnit('RBM + LibSVM Training Event',RestrictedBoltzmannMachine(16,2));
T.Verbose=1;
D_train = Data.DefaultData(rand(1000,16));
Y_train = round(rand(1000,1));

D_val = Data.DefaultData(rand(100,16));
Y_val = round(rand(100,1));

T.set('TrainingEventListener.MaxIterations',10);
T.set('TrainingEventListener',Events.LibSVMProbeTrainingEvent(D_train,Y_train,D_val,Y_val));
T.set('TrainingEventListener.Verbose',true);
T.set('Debug',2);
T.set('TrainingEventListener.ProbeAt',2);
T.set('TrainingEventListener.Settings','-s 0 -t 2');
T.Test(D_train);