T = Test.TestUnit('Gaussian-Bernoulli RBM',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.Verbose=1;
T.Test;

T = Test.TestUnit('Gaussian-Bernoulli RBM + Normalisation [0,1]',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
D = Data.DefaultData(rand(1000,10),DataPreprocessing.Normalisation01);
T.Verbose=1;
T.Test(D);

T = Test.TestUnit('Gaussian-Bernoulli RBM + ICA',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
D = Data.DefaultData(rand(1000,10),DataPreprocessing.ICAPreprocessor);
T.Verbose=1;
T.Test(D);

T = Test.TestUnit('Gaussian-Bernoulli RBM + Whitening',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
D = Data.DefaultData(rand(1000,10),DataPreprocessing.WhiteningPreprocessor);
T.Verbose=1;
T.Test(D);

T = Test.TestUnit('Gaussian-Bernoulli RBM + ZScore',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
D = Data.DefaultData(rand(1000,10),DataPreprocessing.ZscorePreprocessor);
T.Verbose=1;
T.Test(D);

T = Test.TestUnit('Gaussian-Bernoulli RBM + Adaptive Learning Rate',GaussianBernoulliRestrictedBoltzmannMachine(100,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D.Shape = [10,10];
T.Test(D);

T = Test.TestUnit('Gaussian-Bernoulli RBM + Adaptive Learning Rate (Debug 1)',GaussianBernoulliRestrictedBoltzmannMachine(100,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Debug',1);
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D.Shape = [10,10];
T.Test(D);

T = Test.TestUnit('Gaussian-Bernoulli RBM + Adaptive Learning Rate (Debug 2)',GaussianBernoulliRestrictedBoltzmannMachine(100,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Debug',2);
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D.Shape = [10,10];
T.Test(D);

T = Test.TestUnit('Gaussian-Bernoulli RBM + Adaptive Learning Rate (Debug 3)',GaussianBernoulliRestrictedBoltzmannMachine(100,10));
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Debug',3);
T.set('TrainingEventListener.MaxIterations',10);
T.Verbose=1;
D = Data.DefaultData(rand(1000,100),DataPreprocessing.ZscorePreprocessor);
D.Shape = [10,10];
T.Test(D);

T = Test.TestUnit('Gaussian-Bernoulli RBM + ALR + PiecewiseMomentum',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('Momentum',Momentum.PiecewiseMomentum([0.5, 0.9],[1,10]));
T.Verbose=1;
T.Test;

T = Test.TestUnit('Gaussian-Bernoulli RBM + Sparse Regularisation',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.Verbose=1;
T.Test;

T = Test.TestUnit('Gaussian-Bernoulli RBM + Sparse Regularisation + ALR',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.Verbose=1;
T.Test;

T = Test.TestUnit('Gaussian-Bernoulli RBM + Sparse Regularisation + ALR + Update Sigma',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.set('updateSigma',true');
T.Verbose=1;
T.Test;

T = Test.TestUnit('Gaussian-Bernoulli RBM + Sparse Regularisation + ALR + Update Sigma + Lite Kmeans Init',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.set('updateSigma',true');
T.set('SigmaInitialiser',SigmaInitialiser.LiteKMeansInitialiser);
T.Verbose=1;
T.Test;

T = Test.TestUnit('Gaussian-Bernoulli RBM + Sparse Regularisation + ALR + Update Sigma + Internal Kmeans Init',GaussianBernoulliRestrictedBoltzmannMachine(1,1));
T.shortTestSetups;
T.set('Eta',LearningRate.AdaptiveLearningRate(T.machine));
T.set('RegularisationTerm',Regularisation.V2SparseRegularisationTerm);
T.set('updateSigma',true');
T.set('SigmaInitialiser',SigmaInitialiser.InternalKMeansInitialiser);
T.Verbose=1;
T.Test;

T = Test.TestUnit('GBRBM + Dump Training Event',GaussianBernoulliRestrictedBoltzmannMachine(16,2));
T.Verbose=1;
D = Data.DefaultData(rand(1000,16));
D.Shape = [4 4];
T.set('TrainingEventListener.MaxIterations',10);
T.set('TrainingEventListener',Events.DumpTrainingEvent(D));
T.set('TrainingEventListener.Verbose',true);
T.set('TrainingEventListener.DataShape',D.Shape);
T.set('Debug',2);
T.set('TrainingEventListener.ProbeAt',2);
T.Test(D);
%remove image data
!rm Data.*.png
!rm W_*.png

T = Test.TestUnit('GBRBM + LibSVM Training Event',GaussianBernoulliRestrictedBoltzmannMachine(16,2));
T.Verbose=1;
D_train = Data.DefaultData(rand(1000,16));
Y_train = round(rand(1000,1));

D_val = Data.DefaultData(rand(100,16));
Y_val = round(rand(100,1));

T.set('TrainingEventListener.MaxIterations',10);
T.set('TrainingEventListener',Events.LibSVMProbeTrainingEvent(D_train,Y_train,D_val,Y_val));
T.set('TrainingEventListener.Verbose',true);
T.set('Debug',2);
T.set('TrainingEventListener.ProbeAt',2);
T.set('TrainingEventListener.Settings','-s 0 -t 2');
T.Test(D_train);