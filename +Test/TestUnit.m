% TestUnit
%
% This class defines a standard test to check errors on the implementations
%
% Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

classdef TestUnit < matlab.mixin.Copyable
    properties
        TestName = []; %test name
        machine = []   %RBM to train
        Setup = {} %Machine setup to display
        Verbose = 0;
        
        Asserts = {};
    end
    
    properties(SetAccess=protected,GetAccess=public)
        Success = 0;
        Report = [];
    end
    
    methods
        function this = TestUnit(name,machine)
            this.TestName = name;
            this.machine = machine;
        end
        
        function this = set(this,var,value)
            this.Setup(end+1,1:2) = {var,value};
        end
        
        function this = shortTestSetups(this)
            this.machine.TrainingEventListener.MaxIterations = 10;
            this.machine.initialise(10,10);
        end
        
        function this = Test(this,D)
            this.printTestName;
            
            if (~isempty(this.Setup))
                var = this.Setup(:,1);
                [var,idx] = sort(var);

                values = this.Setup(idx,2);

                this.setAndPrintSetup(var,values);
            end
            
            if (~exist('D','var'))
                rng(1);
                D = rand(1000,this.machine.numberOfVisibleUnits);
            end
            
            try
                this.runTestOperations(D);
                this.runAsserts();
                this.Success=1;
            catch E
                this.Report = E;
            end
            
            if (this.Success)
                disp('[SUCCEED]');
            else
                disp('[FAILED]');
                if this.Verbose
                    rethrow(this.Report);
                end
            end
        end
    end
    
    methods(Access=protected)
        
        function this = runAsserts(this)
            for i=1:numel(this.Asserts)
                try
                    s = [];
                    if (isa(this.Asserts{i},'function_handle'))
                        s = func2str(this.Asserts{i});
                        assert(this.Asserts{i}(this.machine));
                    else
                        s = this.Asserts{i};
                        eval(strcat('assert(',this.Asserts{i},');'));
                    end 
                catch E
                    if (strcmp(E.identifier,'MATLAB:assertion:failed'))
                        fprintf('>>> ASSERT FAILED: %s\n',s);
                    end
                    
                    rethrow(E);
                end
            end
        end
        
        function this = runTestOperations(this,D)
            this.machine.Train(D);
        end
        
        function printTestName(this)
            fprintf('> %s:\n',this.TestName);
        end
        
        function this=setAndPrintSetup(this,var,values)
            for i=1:numel(var)
                val = values{i};
                val_str = [];
                
                if (isobject(val))
                    val_str = class(val);
                elseif (isscalar(val))
                    val_str = num2str(val);
                elseif (ischar(val))
                    val_str = val;
                else
                    sz = size(val);
                    tt = arrayfun(@(x)(num2str(x)),sz,'UniformOutput',false);
                    val_str = sprintf('[ %s %s ]',strjoin(tt,'x'),class(val));
                end
                
                eval(strcat('this.machine.',var{i},'=val;'));
                
                fprintf('> %s: %s\n',var{i},val_str);
            end
        end
    end
    
    methods(Static)
        function runAll()
            [path,~,~] = fileparts(mfilename('fullpath'));
            
            l = dir(path);
            
            for i = 1:numel(l)
                if (~ismember(l(i).name,{'.','..'}))
                    fname = l(i).name;
                    [~,scriptname,~] = fileparts(fname);
                    s = strcat('Test.',scriptname);
                    try
                        nargin(s);
                    catch E
                        if (strcmp(E.identifier, 'MATLAB:nargin:isScript'))
                            %it is a script
                            eval([s ';']);
                        end
                    end
                end
            end
        end
    end
end