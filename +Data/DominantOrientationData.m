% DominantOrientationData
%
% This class defines how data should be represented for ERI-RBM
%
% Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% See also: ExplicitRotInvRestrictedBoltzmannMachine, ExplicitRotInvGBRestrictedBoltzmannMachine

classdef DominantOrientationData < Data.DefaultData
    properties
        Bins = [];
    end
    
    methods
        function this = DominantOrientationData(varargin)
            if ((nargin==1) && isa(varargin{1},'Data.DefaultData'))
                this.X = varargin{1}.X;
                this.Preprocessor = varargin{1}.Preprocessor;
                this.Preprocessed = varargin{1}.Preprocessed;
                this.Shape = varargin{1}.Shape;
            else
                if (nargin>=1)
                    this.X = varargin{1};
                end

                if (nargin>=2)
                    this.Preprocessor = varargin{2};
                end
            end
        end
        
        function this = computeDominantOrientation(this,bins,method)
            % Compute the dominant orientation within the dataset
            %
            % SEE ALSO: getDominantOrientation
            
            if (this.Preprocessed)
                warning('Data dominant orientations should be computed before preprocessing');
            end
            
            if (~isempty(this.X))
                this.Bins = Data.DominantOrientationData.getDominantOrientation(...
                                    this.X,...
                                    this.Shape,...
                                    bins,method);
            end
        end
        
        function varargout = subsref(this,S)
            switch S(1).type
                case '()'                    
                    D_out = subsref@Data.DefaultData(this,S);
                    D_out = Data.DominantOrientationData(D_out);
                    D_out.Bins = this.Bins(S.subs{1},:);

                    varargout{1} = D_out;
                case '.'
                   varargout = {builtin('subsref',this,S)};
                case '{}'
                   varargout = {builtin('subsref',this,S)};
            end
        end
    end
    
    methods(Static)
        function psis = getDominantOrientation(X,sz,bins,method)
            % Compute the dominant orientation from the input data
            %
            % Input:
            %   X: data to use
            %   sz: original data shape
            %   bins: histogram bins (e.g. [0, 45, 90, 135, 180, 225, 270, 315, 360])
            %   method: onehot - one-hot encoding; histogram: full gradient 
            %           histrogram
            %
            % Output:
            %   psis: dominant orientation encoded according to method
            
            if (isempty(sz))
                error(['Data shape not specified. A possible cause could be that the parameter '...
                        'Shape of Data.DominantOrientationData class was left empty. You have to '...
                        'specify the original size of the images (or patches) of your dataset in '...
                        'terms of height x width']);
            end
                
            if isscalar(bins)
                bins = linspace(0,360,bins+1);
            end
                         
            M = max(bins);
            m = min(bins);
            N = numel(bins)-1;
            
            angle_bins = bins - (M-m)/N/2; 
                  
            psis = zeros(size(X,1),N);          
            g = fspecial('gaussian',sz(1:2),min(sz(1:2))/5);
            
            ch = 1;
            
            if (length(sz)>2)
                ch = sz(3);
            else
                sz(3)=1;
            end
            
            gf = normpdf(1:N,ceil(N/2),0.5);
            gf = circshift(gf,ceil(N/2),2);
            
            for t = 1:size(psis,1);

                I = reshape(X(t,:),sz(1), sz(2),sz(3));

                if (size(I,3)>1)
                    I = rgb2gray(I);
                end
                
                [gMag,gDir] = imgradient(I);
                gDir(gDir<0) = 360 + gDir(gDir<0);
                
                gMag = gMag .* g;
                
                [histogram,~] = MiscFunctions.histwc(gDir(:),gMag(:),bins);
                
                switch (method)
                    case 'onehot'
                        [~,i] = max(histogram);
                        psis(t,i) = 1;
                    case 'fuzzyoh'
                        [~,i] = max(histogram);
                        psis(t,:) = circshift(gf,i-1,2);
                    case 'histogram'
                        psis(t,:) = histogram./sum(histogram);
                    otherwise
                        error('Invalid orientation estimation method');
                end
                
                
            end
        end
    end
end