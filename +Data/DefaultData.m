% DefaultData
%
% This class defines how data has to be represented within an RBM
%
% Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% See also: RestrictedBoltzmannMachine, GaussianBernoulliRestrictedBoltzmannMachine
    
classdef DefaultData < handle & matlab.mixin.SetGet & matlab.mixin.Copyable
    properties
        X = [] %Dataset
        Preprocessor = DataPreprocessing.EmptyPreprocessor; %Data preprocessor
        Shape = []; %Data Shape (for images only)
    end
    
    properties(SetAccess='protected', GetAccess='public')
        Preprocessed = false; %Keeps track whether the data have been preprocessed or not
    end
    
    methods
        function this = DefaultData(varargin)
            % Constructor
            %
            % Input
            %   Train : train data (optional)
            %   Preprocessor: data preprocessor (optional)
            %
            
            if (nargin>=1)
                this.X = varargin{1};
            end
            
            if (nargin>=2)
                this.Preprocessor = varargin{2};
            end
        end
        
        function this = preprocess(this)
            % preprocess
            %
            % preprocess train/validation/train sets
            %
            if (~this.Preprocessed)
                this.X = this.Preprocessor.preprocessData(this.X);
                this.Preprocessed=true;
            end
        end
        
        function varargout = size(this,dim)
            %Returns the number of samples in the dataset
            
            if ((nargin>1)&& (dim>1))
                varargout{1} = 1;
            else
                varargout{1} = size(this.X,1);
            end
        end
        
        
        function X = data(this)
            %legacy function
            %
            %SEE ALSO: getData
            
            X = this.getData;
        end
        
        function X = getData(this)
            %Return the data
            X = this.X;
        end
        
        function this = copyToDevice(this)
            this.X = gpuArray(this.X);
        end
        
        function varargout = subsref(this,S)
            switch S(1).type
                case '()'
                    if (numel(S.subs)>1)
                        if (~ischar(S.subs{2}) && (S.subs{2} >1))
                            error('Array index out of bounds');
                        end
                    end
                    
                    D_out = Data.DefaultData;
                    D_out.Preprocessor = this.Preprocessor;
                    
                    D_out.X = this.X(S.subs{1},:);
                           
                    varargout{1} = D_out;
                case '.'
                   varargout = {builtin('subsref',this,S)};
                case '{}'
                   varargout = {builtin('subsref',this,S)};
            end
        end
        
        function varargout = subsasgn(this,S,obj)
            switch S(1).type
                case '()'
                    if (numel(S.subs)>1)
                        if (~ischar(S.subs{2}) && (S.subs{2} >1))
                            error('Array index out of bounds');
                        end
                    end
                    
  
                    if (ischar(S.subs{1})&&strcmp(S.subs{1},':'))
                        S.subs{1} = 1:max(size(this.X,1),size(obj,1));
                    end
                    
                    if (isscalar(obj))
                        this.X(S.subs{1},:) = obj;
                    else
                        this.X(S.subs{1},:) = obj(1:numel(S.subs{1}),:);
                    end
                            
                    varargout{1} = this;
                case '.'
                   varargout = {builtin('subsasgn',this,S,obj)};
                case '{}'
                   varargout = {builtin('subsasgn',this,S,obj)};
            end
        end
        
        function X = plus(a,b)
            if (isa(a,'Data.DefaultData'))
                a=a.data;
            end
            
            if (isa(b,'Data.DefaultData'))
                b=b.data;
            end
            
            X = a + b;
        end
        
        function X = minus(a,b)
            if (isa(a,'Data.DefaultData'))
                a=a.data;
            end
            
            if (isa(b,'Data.DefaultData'))
                b=b.data;
            end
            
            X = a - b;
        end
        
        function X = times(a,b)
            if (isa(a,'Data.DefaultData'))
                a=a.data;
            end
            
            if (isa(b,'Data.DefaultData'))
                b=b.data;
            end
            
            X = a .* b;
        end
        
        function X = mdivide(a,b)
            if (isa(a,'Data.DefaultData'))
                a=a.data;
            end
            
            if (isa(b,'Data.DefaultData'))
                b=b.data;
            end
            
            X = a ./ b;
        end
        
        function X = ldivide(a,b)
            if (isa(a,'Data.DefaultData'))
                a=a.data;
            end
            
            if (isa(b,'Data.DefaultData'))
                b=b.data;
            end
            
            X = a .\ b;
        end
    end
end

