% Explicit Rotation Invariant Restricted Boltzmann Machine
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% References
% [1] M. V. Giuffrida and S. A. Tsaftaris, "Rotation-Invariant Restricted
%     Boltzmann Machine using shared gradient filters," p. 8, Apr. 2016.
%
% See also: RestrictedBoltzmannMachine

classdef ExplicitRotInvRestrictedBoltzmannMachine < RestrictedBoltzmannMachine
    properties       
        NumberChannels = 1; %number of channels in the image (e.g. 1 grayscale, 3 rgb)
        
        NumberOfBins = 18; %Number of bins that the histogram of oriented gradients has
        
        
        KernelSize = 3; %Guassian kernel size that spread the contribution of activations
        GaussianSigma = 1; %Gaussian sigma parameter
        OrientationMethod = 'onehot'; %Orientation Estimation: onehot: one single dominant orientation; histogram: full histogram of orientation
    end
    
    properties(Hidden,SetAccess=protected,GetAccess=public)
        AngleBins = [];
    end
    
    properties(Hidden,Access=protected)
        RotationMatrices = [];
        DataShape=[]; %size of the input image (redundant with Data, but users may forget it)
    end
    
    
    methods
        function this = ExplicitRotInvRestrictedBoltzmannMachine(varargin)
            %Constructor for a Rotation Invariant RBM
            %   r_v: number of rows in the visible layer
            %   c_v: number of columns in the visible layer
            %   ch: number of channels (optional)
            %   h: number of hidden units
            
            
            if (nargin>0)
                r_v = varargin{1};
                c_v = varargin{2};
                
                if (nargin<=3)
                    h = varargin{3};
                else
                    this.NumberChannels = varargin{3};
                    h = varargin{4};
                end
                
                this.initialise(r_v*c_v*this.NumberChannels,h);
                
                this.RegularisationTerm = Regularisation.NoRegularisation(this);
                this.setupBins;
                
                this.DataShape = [r_v,c_v,this.NumberChannels];
            end
            
            if (~isdeployed && ~exist('mtimesx','file'))
                fname = mfilename('fullpath');
                [path,~] = fileparts(fname);
                
                addpath(fullfile(path,'libraries','mtimesx'));
            end
        end
        
        function this = initialise(this,v,h)
            this = initialise@RestrictedBoltzmannMachine(this,v,h);
            this.setupBins;
        end
        
        function this = set.NumberOfBins(this,n)
            this.NumberOfBins = n;
            this.setupBins(size(this.W,3)~=n);
        end
        
        function this=Train(this,X)
            if (isa(X,'Data.DominantOrientationData'))
                if (isempty(X.Shape))
                    X.Shape=this.DataShape;
                end
            else
                X = Data.DominantOrientationData(X);
                X.Shape = [this.DataShape, this.NumberChannels];
            end
            
            if (isempty(X.Bins))
                X.computeDominantOrientation(this.AngleBins,this.OrientationMethod);
            end
            
            Train@RestrictedBoltzmannMachine(this,X);
        end
        
        function this=TrainZ(this,X)
            if (isa(X,'Data.DominantOrientationData'))
                if (isempty(X.Shape))
                    X.Shape=this.DataShape;
                end
            else
                X = Data.DominantOrientationData(X);
                X.Shape = this.DataShape;
            end
            
            if (isempty(X.Bins))
                X.computeDominantOrientation(this.AngleBins,this.OrientationMethod);
            end
            
            X.preprocess;
            
            H = zeros(size(X.X,1),model.numberOfHiddenUnits,model.NumberOfBins);

            b = X.Bins;
            X.Bins= [];

            for i=1:model.NumberOfBins
                h = model.probHgivenV(X,model.W(:,:,i));
                H(:,:,i) = h.data;
            end

            X.Bins = b;
            [classes,~] = find(b');
            
            this.v = zeros(1,this.NumberOfBins);
            
            for t=1:100
                %computing j
                s = zeros(size(D_train.X,1),model.NumberOfBins);
                for i=1:model.NumberOfBins
                    s(:,i) = my_softmax(bias,H,ones( size(D_train.X,1),1)*i);
                end
                
                J = - mean( sum(D_train.Bins.*log(s),2));
                
                delta_B = mean(s - D_train.Bins)*lambda;
                bias = bias - delta_B;
                
                if (this.Debug>0)
                    fprintf('Value function: %f\n',J)
                end
                
            end
        end
        
        function bin = computeDominantRotation(this,V)
            %Compute dominant direction of each patch
            % V: samples in the visible layer
            
            if (this.Debug > 0)
                disp('Computing Dominant Orientation');
            end
            
            bin = ExplicitRotInvRestrictedBoltzmannMachine.getDominantDirection(V,...
                this.DataShape,...
                this.AngleBins,...
                this.OrientationMethod);
        end
        
        function H = extractFeatures(this,X)
            if (~X.Preprocessed)
                X.preprocess;
            end
            
            if (strcmp(class(X),'Data.DefaultData') || isempty(X.Bins))
                [~,H] = this.probZgivenV(X);
            else
                H = this.probHgivenV(X,this.W);
                H = H.data;
            end
        end
        
        function H = probHgivenV(this,V,W)
            %Computes the p(h|v) probabilities
            % V: visibile layer
            % W: weight matrix (default this.W)
            % Output
            %   H: activation probabilities
            
            if (nargin<3)
                W = this.W;
            end
            
            if (strcmp(class(V),'Data.DefaultData') || isempty(V.Bins))
                H = probHgivenV@RestrictedBoltzmannMachine(this,V,W);
            else
                
   
                H = zeros( size(V.data,1) , this.numberOfHiddenUnits,this.NumberOfBins);
                
                if (this.Cuda)
                    H = gpuArray(H);
                end
                
                for i=1:this.NumberOfBins
                    Weights = W(:,:,i);
                    
                    p = probHgivenV@RestrictedBoltzmannMachine(this,V,Weights);
                    M = repmat(V.Bins(:,i),1,size(p.data,2));
                    
                    H(:,:,i) = p.data.*M;
                end
                
                H = sum(H,3);
                H = min(H,1); % gradients might be an epsilon greater than 1
                
                H_raw = H;
                H = V.copy();
                H.X=H_raw;
            end
        end
        
        
        function V = probVgivenH(this,H)
            %Computes the p(v|h) probabilities
            % V: visibile layer
            % Output
            %   H: activation probabilities
            
            V = zeros( size(H.data,1) , this.numberOfVisibleUnits,this.NumberOfBins);
            
            if (this.Cuda)
                V = gpuArray(V);
            end
            
            for i=1:this.NumberOfBins
                Weights = this.W(:,:,i);
                
                
                p = probVgivenH@RestrictedBoltzmannMachine(this,H,Weights);
                
                M = repmat(H.Bins(:,i),1,size(p.data,2));
                
                V(:,:,i) = p.data.*M;
            end
            
            V = sum(V,3);
            
            V_raw = V;
            V = H.copy();
            V.X=V_raw;
        end
        
        function [Z,H] = probZgivenV(this,V)
            % Infers the rotation of a patch
            %
            % Input:
            %   V: Data to infer
            %
            % Output:
            %   Z: probabilities of estimated rotations
            %   H: activations of V
            
            H = cell(1,this.NumberOfBins);
            
            if (isa(V,'Data.DominantOrientationData'))
                oldB = V.Bins;
                V.Bins = [];
            end
            
            for i=1:numel(H)
                H{i} = this.probHgivenV(V,this.W(:,:,i));
                H{i} = H{i}.data;
            end
            
            sparsity = cellfun(@(x)(mean(x,2)),H,'UniformOutput',false);
            sparsity = cat(2,sparsity{:});
            
            H = cat(3,H{:});
            
            [~,Z] = min(abs(sparsity-this.ActualSparsity),[],2);
            
            if (nargout>1)
                H2 = zeros(size(H(:,:,1)));

                for i=1:numel(Z)
                    H2(i,:) = H(i,:,Z(i));
                end
                
                H = H2;
            end
            
            if (isa(V,'Data.DominantOrientationData'))
                V.Bins = oldB;
            end
   
        end
        
        
        function this = SetPretrainedW(this,W)
            this.W(:,:,1) = W;
            for i=1:this.NumberOfBins-1
                this.W(:,:,i+1) = this.RotationMatrices(:,:,i) * W;
            end
        end
    end
    
    
    methods(Access=protected)
        function nabla = computeGradients(this,v0_drawn,h0_prob,v1_drawn,h1_prob)
            % Computes gradient of the CD algorirthm
            % Input:
            %  see output Gibbs Sampling
            %
            % Output:
            %   nabla: Gradients of the parameters
            
            nabla = computeGradients@RestrictedBoltzmannMachine(this,v0_drawn,h0_prob,v1_drawn,h1_prob);
            nabla = this.computeERIGradients(nabla,v0_drawn,h0_prob,v1_drawn,h1_prob);
        end
        
        function nabla = computeERIGradients(this,nabla,v0_drawn,h0_prob,v1_drawn,h1_prob)
            % Computes shared filter gradients for the W matrix
            % Input:
            %  see output Gibbs Sampling
            %
            % Output:
            %   nabla: Gradients of the parameters
            

            %preparing
            nablaW     = zeros(this.numberOfVisibleUnits,this.numberOfHiddenUnits,this.NumberOfBins);
            nablaW_rot = zeros(size(nablaW));
            
            if (this.Cuda)
                nablaW = gpuArray(nablaW);
                nablaW_rot = gpuArray(nablaW_rot);
            end
            
            %rotation angles (deg)
            theta = cumsum(ones(1,this.NumberOfBins-1) * (360/this.NumberOfBins));
            
            
            N = size(v0_drawn.data,2);
            for t = 1:this.NumberOfBins
                weights = transpose(repmat(v0_drawn.Bins(:,t),1,N));

                nablaW(:,:,t) = (weights.*v0_drawn.data' * h0_prob.data) - (weights.*v1_drawn.data' * h1_prob.data);
            end
            
            nablaWStacked = reshape(nablaW,size(nablaW,1),[]);
            for j=1:numel(theta)
            %  t = theta(j);
                u = mod((1:this.NumberOfBins)+j-1,this.NumberOfBins) + 1;
                
             %  updateFilters = reshape(nablaW,this.DataShape(1),this.DataShape(2),[]);
                
              % if (this.Cuda)
              %     updateFilters=im2single(updateFilters);
              % end
                
              %updateFiltersRot = imrotate(updateFilters,t,'bilinear','crop');
               
             %nWr = reshape(updateFiltersRot,this.numberOfVisibleUnits,this.numberOfHiddenUnits,[]);
                
                updateFiltersRot = this.RotationMatrices(:,:,j) * nablaWStacked;
                nWr = reshape(updateFiltersRot,size(nablaW_rot(:,:,u) ));

                
                nablaW_rot(:,:,u) = nablaW_rot(:,:,u) + nWr;
            end
            
            nabla.W = nablaW + nablaW_rot;
        end
        
        function this = setupBins(this,initW)
            % Set the bins for with equidistant angles
            % Input
            %   initW: (optional) initialise the W matrix (default true)
            
            if (nargin<2)
                initW=true;
            end
            
            if (initW)
                this.W = repmat(this.W(:,:,1),1,1,this.NumberOfBins);
            end
            
            this.AngleBins = linspace(0,360,this.NumberOfBins+1);
            
            theta = cumsum(ones(1,this.NumberOfBins-1) * (360/this.NumberOfBins));
            
            c = this.NumberChannels;
            this.RotationMatrices = MiscFunctions.get_txmat_rot(sqrt(this.numberOfVisibleUnits/c),c,-deg2rad(theta));
            this.RotationMatrices = cellfun(@(T)(full(T)),this.RotationMatrices,'UniformOutput',false);
            this.RotationMatrices = cat(3,this.RotationMatrices{:});

            if (this.Cuda)
                this.RotationMatrices = gpuArray(this.RotationMatrices);
            end
        end
        
        function data = dataToDump(this)
            data = dataToDump@RestrictedBoltzmannMachine(this);
            
            keys   = {'NumberOfBins','OrientationMethod'}';
            values = MiscFunctions.readProperties(this,keys);
            
            data = cat(1,data,cat(2,keys,values));
        end
    end
    
    
    
    methods(Static)
        function E = freeEnergy(varargin)
            if ((nargin>1) && isa(varargin{2},'ExplicitRotInvRestrictedBoltzmannMachine'))
                [X,r] = deal(varargin{:});
                E = ExplicitRotInvRestrictedBoltzmannMachine.freeEnergy(X,r.W,r.c,r.b);
            else
                
                [X,W,c,b] = deal(varargin{:});
                
                nBins = size(X.Bins,2);
                
                Es = zeros(size(X.data,1),nBins);
                
                for i=1:nBins
                    %in order to recycle the freeEnergy defined in the
                    %superclass, b and X must be multiplied by the histogram of
                    %the dominant orientations
                    
                    D =     X.data .* repmat(X.Bins(:,i),1,size(X.data,2));
                    hbias = X.Bins(:,i) * b;
                    
                    visible_term = D * c';
                    hidden_term = sum(log(1+exp(hbias+D*W(:,:,i))),2);
                    
                    Es(:,i) = gather(-visible_term - hidden_term);
                end
                
                E = sum(Es,2);
            end
        end
    end
    
end