% TI RBM interface
%
% This class does not implement TI RBM by Sohn ICML 2012, but provide
% an interface to run it
%
% Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% References
%   [1] https://github.com/kihyuks/icml2012_tirbm#learning-invariant-representations-with-local-transformations
%
% See also:
% RestrictedBoltzmannMachine,GaussianBernoulliRestrictedBoltzmannMachine


classdef TIRestrictedBoltzmannMachine < ExternalRestrictedBoltzmannMachine
    properties (Constant,Access=private,Hidden)
        url = 'https://github.com/kihyuks/icml2012_tirbm.git';
        folder_name = 'tirbm_sohn_icml2012'
    end
    
    properties
        Parameters = [];
        History = [];
    end
    
    properties(Access=protected,Hidden)
        T = [];
    end
    
    methods
        function this = TIRestrictedBoltzmannMachine
            %this.Parameters.rs = 6;
            this.Parameters.numvis = 100;
            this.Parameters.rSize = this.Parameters.numvis;
            this.Parameters.numhid = 100;
            this.Parameters.optgpu = false;
            this.Parameters.grid = 1; %unknown
            this.Parameters.txtype = 'rot';
            this.Parameters.numtx =18;
            this.Parameters.numrot = 18;
            this.Parameters.pbias = 0.1; %sparsity target
            this.Parameters.plambda = 3; %sparsity lambda
            this.Parameters.intype = 'real';
            this.Parameters.sptype = 'exact';
            this.Parameters.savepath = './';
            this.Parameters.dataset='noname';
            this.Parameters.ws=10;
            
            % other hyper parameters
            this.Parameters.maxiter = 100;
            this.Parameters.batchsize = 100;
            this.Parameters.epsilon = 0.001;
            this.Parameters.eta_sigma = 0;
            this.Parameters.l2reg = 0.2; %unknown
            this.Parameters.epsdecay = 0;
            this.Parameters.kcd = 1;
            this.Parameters.numch = 1;
            
            this.Parameters.momentum=[0.9 0.9];
            
            this.Status = 'initialised';
        end
        
        function this = numberOfRotations(this,n)
            this.Parameters.numtx = n;
            this.Parameters.numrot = n;
        end
        
        function this=Train(this,X)
            
            if (~X.Preprocessed)
                X.preprocess;
            end
            
            window_size = sqrt(size(X.X,2));
            
            if ((window_size - floor(window_size))>0)
                error('TI RBM allows only squared input images');
            end
            
            this.Parameters.ws = window_size;
            
            this.T = get_txmat_rot(this.Parameters.ws, this.Parameters.numch,...
                     deg2rad(linspace(0,360,this.Parameters.numrot+1)));
                 
            this.T = cellfun(@(x)(full(x)),this.T,'UniformOutput',false);
            
            
            [weight, params, history] = tirbm_train(X.X', this.Parameters, this.T);
            
            weight = gpu2cpu_struct(weight);
            history = gpu2cpu_struct(history);
            
            this.Parameters = params;
            this.externalModel = weight;
            this.History = history;
            
            this.Status='trained';
        end
        
        function H = extractFeatures(this,X)
            if (strcmp(this.Status,'trained'))
                A = zeros(size(this.T{1}, 1)*this.Parameters.numtx, size(this.T{1}, 2));

                for i = 1:this.Parameters.numtx,
                    A((i-1)*size(this.T{1}, 1)+1:i*size(this.T{1}, 1), :) = double(full(this.T{i}));
                end
                
                if (X.Preprocessed)
                    X.preprocess;
                end
                
                hidprob  = zeros(this.Parameters.numhid, this.Parameters.numtx+1, size(X.X,1));
                hbiasmat = repmat(this.externalModel.hidbias, [1 size(X.X,1)]);

                [H,~] = TIRestrictedBoltzmannMachine.tirbm_inference_sub(A, X.data',...
                    this.externalModel.vishid, hbiasmat, hidprob,...
                    this.Parameters.numvis, this.Parameters.numtx, ...
                    this.Parameters.numhid);
                H = H';
            else
                error('Machine not trained');
            end
        end
        
        function n = numberOfHiddenUnits(this)
            n = this.Parameters.numhid;
        end
        
        function n = numberOfVisibleUnits(this)
            n = this.Parameters.numvis;
        end
    end
    
    methods(Static)
        function [hidprob_mult, hidprob] = tirbm_inference_sub(Tlist, xb, vishid, hbiasmat, hidprob, numvis, numtx, numhid)

            batchsize = size(xb, 2);

            Tx = reshape(Tlist*xb, numvis, numtx*batchsize);
            hidprob = reshape(hidprob, numhid, numtx+1, batchsize);
            hidprob(:, 1:numtx, :) = reshape(vishid'*Tx + hbiasmat, numhid, numtx, batchsize);
            hidprob(:, numtx+1, :) = 0;
            hidprob = exp(bsxfun(@minus, hidprob, max(hidprob, [], 2)));
            hidprob = bsxfun(@rdivide, hidprob, sum(hidprob, 2));
            hidprob_mult = double(squeeze(sum(gather(hidprob(:,1:numtx,:)),2)));
        end
        
        function this2 = loadobj(this)
            TIRestrictedBoltzmannMachine.addpathExternalLib;
            this2=this;
        end
        
        function install(varargin)
            dir = TIRestrictedBoltzmannMachine.checkDir;
            
            dir = fullfile(dir,TIRestrictedBoltzmannMachine.folder_name);
            
            command = sprintf('git clone %s %s',TIRestrictedBoltzmannMachine.url,dir);
            
            t = system(command);
            
            if (t>0)
                error('Cannot install TIRBM. Run `%s` externally (e.g. terminal), namely outside MATLAB command line',command);
            end
        end
        
        function applyPatch()
            dir = TIRestrictedBoltzmannMachine.checkDir;
            old = pwd;
            cd(fullfile(dir,TIRestrictedBoltzmannMachine.folder_name));
            system(sprintf('git apply -v %s/../patches/tirbm.patch',dir),'-echo');
            cd(old);
        end
    end
    
    methods(Static,Access=protected)
        
        function addpathExternalLib
            dir = TIRestrictedBoltzmannMachine.checkDir;
            dir = fullfile(dir,TIRestrictedBoltzmannMachine.folder_name);
            
            if (~exist(dir,'dir'))
                error('TIRBM not installed. Run TIRestrictedBoltzmannMachine.install');
            end
            
            addpath(dir);
            addpath(fullfile(dir,'utils'));
        end
    end
end