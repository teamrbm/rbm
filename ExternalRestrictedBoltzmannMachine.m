% Generic interface to use external RBM implementations
%
% Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% Coded inspired from:
%   - http://www.mathworks.com/matlabcentral/fileexchange/42853-deep-neural-network
%   - http://www.kyunghyuncho.me/home/code
%
% References
%  [1] Hinton, G. (2010)."A Practical Guide to Training Restricted Boltzmann Machines.
%      Computer, 9(3), 1. doi:10.1007/978-3-642-35289-8_32
%
% See also:
% RestrictedBoltzmannMachine,GaussianBernoulliRestrictedBoltzmannMachine
%


classdef (Abstract) ExternalRestrictedBoltzmannMachine < AbstractRestrictedBoltzmannMachine
    properties
        externalModel = [];
    end
    
    methods
        
        function this = ExternalRestrictedBoltzmannMachine
            this.addpathExternalLib;
        end
    end
    
    methods(Access=protected,Static)
        function installdir = checkDir()
            [dir,~,~] = fileparts(which('ExternalRestrictedBoltzmannMachine'));
            
            installdir= fullfile(dir,'external');
            
            if (~exist(installdir,'dir'))
                mkdir(installdir);
            end
        end
        
        addpathExternalLib();        
    end
    
    methods(Static)
        install(varargin);
    end
    
end