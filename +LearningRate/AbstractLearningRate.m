% Abstract Learning Rage
% 
% This class defines how learning rate should evolve along time
% 
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%

classdef (Abstract) AbstractLearningRate < handle & matlab.mixin.Copyable
    properties
        RBM;
        Value=0.00001;
    end
    
    properties(SetAccess=protected,GetAccess=public)
        Etas = [];
    end
    
    methods
        function this = AbstractLearningRate(rbm,varargin)
            if (isa(rbm,'AbstractRestrictedBoltzmannMachine'))
                this.RBM = rbm;
                if nargin>1
                    this.Value = varargin{1};
                end
            end
        end
        
        % getLearningRate
        %
        % Compute the current learning rate
        %
        % Input:
        %   D: Input dataset
        %   T: current epoch
        %   i: current minibatch
        %   nabla: current computed gradients
        %   mom: current momentum
        %   v0,h0: positive phase visibile and
        %           hidden layers (probabilities and
        %           drawn samples)
        %   v1,h1: negative phase
        %
        % Output:
        %   Eta: current learning rate
        Eta = getLearningRate(this,D,T,i,mom,v0,h0,v1,h1)
    end
end