% Adaptive Learning Rage
% 
% This class computes a suitable learning rate
% 
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% Reference(s):
%   [1] Cho,K.: Improved LearningAlgorithms forRestrictedBoltzmann Machines.
%               Master?s thesis, Aalto University School of Science (2011)

classdef AdaptiveLearningRate < LearningRate.AbstractLearningRate
    properties
        AdaptiveLearningRateCandidates = [0.99 1.01]; 
        Bounds = struct('Upper',Inf,'Lower',-Inf);
    end
    
    methods
        function this = AdaptiveLearningRate(varargin)
            this = this@LearningRate.AbstractLearningRate(varargin{:});
        end
        
        function Eta = getLearningRate(this,D,~,i,nabla,momentum,~,~,v1,~)
            etas = [this.AdaptiveLearningRateCandidates 1] * this.Value;
            v0_next = this.RBM.getBatch(D,i+1);

           E_v1_before = this.RBM.freeEnergy(v1,this.RBM);

            cost = zeros(1,numel(etas));
            

            for k=1:numel(cost)
                cand = etas(k)/this.RBM.MiniBatchSize;
                nabla_tmp = this.RBM.applyMomentum(nabla,cand,momentum);

                r = this.RBM.applyGradients(nabla_tmp);
                updatedMachine = struct2cell(r);

                E_v0n_after = this.RBM.freeEnergy(v0_next,updatedMachine{:});
                E_v1_after  = this.RBM.freeEnergy(v1,updatedMachine{:});

                cost(k) = sum(-E_v0n_after - MiscFunctions.logsum(double(gather(-E_v1_after + E_v1_before))) + log(this.RBM.MiniBatchSize));
            end

            [~,p] = max(cost);
            learningRate = etas(p);
            this.Value = learningRate;
            Eta = this.Value;
            
            Eta = max(Eta,this.Bounds.Lower);
            Eta = min(Eta,this.Bounds.Upper);
            
            if (this.RBM.Debug>=1)
                this.Etas = [this.Etas Eta];
            end
        end
    end
end