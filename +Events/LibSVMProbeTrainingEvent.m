%  LibSVMProbeTrainingEvent
%
%   This class fit an SVM and classify using a validation set
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%

classdef  LibSVMProbeTrainingEvent < Events.AbstractProbeTrainingEvent
    properties
        Settings = '-s 0 -t 2 -q';
    end
   
    methods
        function this = LibSVMProbeTrainingEvent(varargin)
            this = this@Events.AbstractProbeTrainingEvent(varargin{:});
            Events.LibSVMProbeTrainingEvent.checklibSVM;
        end
        
        
        function Probe(this,rbm,t)
            H_train = rbm.extractFeatures(this.X_train);
            H_val   = rbm.extractFeatures(this.X_val);
            if (isstruct(H_train))
                H_train = H_train.data;
            end
            
            if (isstruct(H_val))
                H_val = H_val.data;
            end
            
            model = svmtrain(this.Y_train,H_train,this.Settings);
            
            [~,acc_train,~] = svmpredict(this.Y_train,H_train,model,'-q');
            [~,acc_val,~  ] = svmpredict(this.Y_val,H_val,model,'-q');
            
            %this.Results = cat(1,Results, [t,acc_train(1),acc_val(1)]);
            
            this.TrainingResults = [ this.TrainingResults acc_train(1) ];
            this.ValidationResults = [ this.ValidationResults acc_val(1) ];
            
            if (this.PlotResults)
                this.Plot(acc_train(1),acc_val(1),t);
            end
            
        end
    end
    
    methods(Static,Access=protected)
        function checklibSVM()
            trainFun = which('svmtrain');
            testFun  = which('svmpredict');
            
            if ( isempty(trainFun) || isempty(testFun))
                error(['LibSVM is not installed in your system. Please, visit ' ...
                      '<a href="https://www.csie.ntu.edu.tw/~cjlin/libsvm/">https://www.csie.ntu.edu.tw/~cjlin/libsvm/</a> '...
                      'and follow the instructions']);
            end
        end
    end
end
