%  DumpTrainingEvent
%
%   This class saves results as images periodically
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef DumpTrainingEvent < Events.AbstractProbeTrainingEvent
    properties
        Data = 1:16; %Data reconstruction show (indeces)
        Plots = {'RMSE'}; %Which field to plot
        
        Folder = './'; %Where dumps are saved
        
        FilenameW = 'W_<T>.S<S>.png';
        FilenameData = 'Data.<T>.png';
        
        DumpW = true;
        DumpData = true;
        
        DataShape = [];
    end
    
    methods
        
        function this = DumpTrainingEvent(varargin)
            
            for i=nargin+1:4
                varargin{i} = [];
            end
            
            this = this@Events.AbstractProbeTrainingEvent(varargin{:});
        end
        
        function Probe(this,rbm,t)
            if (this.DumpW)
                for s = this.W.Slices
                    I = MiscFunctions.montage(reshape(rbm.W(:,:,s),this.DataShape(1),this.DataShape(2),[]),...
                                              this.W.Filters);
                    
                    f = strrep(this.FilenameW,'<T>',num2str(t));
                    f = strrep(f,'<S>',num2str(s));
                    
                    imwrite(I,fullfile(this.Folder,f));
                end
            end
            
            if (this.DumpData && (rbm.Debug>1))
                X  = reshape(rbm.LastReconstructionData.X(this.Data,:)',this.DataShape(1),this.DataShape(2),[]);
                Xr = reshape(rbm.LastReconstructionData.Xr(this.Data,:)',this.DataShape(1),this.DataShape(2),[]);
                
   
                I1 = MiscFunctions.montage(X);
                I2 = MiscFunctions.montage(Xr);
                
                f = strrep(this.FilenameData,'<T>',num2str(t));
                
                imwrite(cat(2,I1,I2),fullfile(this.Folder,f));
            elseif (this.DumpData && (rbm.Debug<=1))
                warning('Set Debug>=2 of your RBM to dump reconstruction data');
            end
        end
    end
end

