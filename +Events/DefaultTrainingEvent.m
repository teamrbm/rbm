%  DefaultTrainingEvent
%
%   This class defines defaults events for RBM training
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef DefaultTrainingEvent < Events.AbstractTrainingEvent
    properties
        Verbose=false;  % print status in the console
        PlotData = false; %plot rmse and free energy over time
        ShowWeightMatrix = false; %show weightmatrix
        
        W = struct('Filters',16,'Slices',1,'SubPlot',[1 1]);
        
        MaxIterations = 100;
        
        SaveRBM=false;
        SaveDir = './';
        SaveFile = 'mymodel.<T>.mat';
        
    end
    
%     properties (Access=protected)
%         CurrentFigure = [];
%         CurrentAxes = [];
%     end
    
    methods
        function this=DefaultTrainingEvent()
        end
        
        function stop = StopCriterion(this,~,t)
            stop = t>this.MaxIterations;
        end
        
        function MiniBatchResults(this,~,t,i,n)                        
            if this.Verbose
                if (i==1)
                    MiscFunctions.textprogressbar(sprintf('\nEpoch %d\t ',t))
                end
                MiscFunctions.textprogressbar(i./n*100);
            end
        end
        
        function EpochResults(this,rbm,t)
            if this.PlotData
                this.ShowData(rbm,t);
            end
            
            if this.ShowWeightMatrix
                this.ShowW(rbm);
            end
            
            if (rbm.Debug > 0) && this.Verbose;
                T_mse = min (t,length(rbm.RMSE));
                T_et  = min (t,length(rbm.ElapsedTime));
                
                [sz,~] = rbm.getMemorySize();
                
                msg = sprintf(['\n Recon. Error: %f \tSparsity: %f \t Eta: %f\tElapsed Time: %s\tMem: %.2fMB'],...
                    rbm.RMSE(T_mse),...
                    mean(rbm.Sparsity),...
                    rbm.Eta.Value,...
                    datestr(datenum(0,0,0,0,0,rbm.ElapsedTime(T_et)),'HH:MM:SS'), ...
                    round(sz./(1024.^2),2));
                
                if (rbm.isGPUActive)
                    dev = gpuDevice;
                    delta = dev.TotalMemory - dev.AvailableMemory;
                    msg = sprintf('%s\tGPU Mem: %.2fMB',msg,round(delta./(1024.^2),2));
                end
%                 if (~isnan(peak))
%                     msg = sprintf('%s\tPeak Mem: %.2fMB',msg,round(peak./(1024.^2),2));
%                 end

                
                
                MiscFunctions.textprogressbar(msg);
            end
        end
        
        function TrainingEnded(this,rbm)
            if rbm.Debug>0 && this.Verbose
                disp('Training ended');
            end
        end
    end
        
    methods(Access=protected)
        
        function ShowW(this,rbm)
            persistent Figure;
            maxSlice = min(this.W.Slices,size(rbm.W,3));
            maxFilters = min(this.W.Filters, rbm.numberOfHiddenUnits);

            if (isempty(Figure) || ~ishandle(Figure))
                Figure = figure;
            end

            set(0, 'CurrentFigure', Figure)

            for i=1:maxSlice
                h=subplot(this.W.SubPlot(1),this.W.SubPlot(2),i);
                T = rbm.W(:,1:maxFilters,i);

                I = reshape(T,rbm.OriginalDataShape(1),rbm.OriginalDataShape(2),1,[]);
                montage(I,'Parent',h,'DisplayRange',[min(T(:)) max(T(:))]);
            end
            drawnow;
        end
        
        function ShowData(this,rbm,t)
            persistent AnimatedLines;
            persistent CurrentFigure;
            
            newPlot = false;
            if isempty(CurrentFigure) || ~ishandle(CurrentFigure)
                CurrentFigure = figure;
                AnimatedLines = [];

                newPlot = true;
            end

            %if there are no axes, create new ones

            nSubplots = 2;

            if (rbm.AdaptiveLearningRate)
                nSubplots=nSubplots+1;
            end

            %plot the status up to now
            figure(CurrentFigure);
            for i = 1 : nSubplots
                subplot(nSubplots,1,i);


                arr = [];

                switch i
                    case 1
                        arr = rbm.RMSE(1:t);
                    case 2
                        arr = rbm.Energies(1:t);
                    case 3
                        arr = rbm.Etas(1:t);
                end

                %if the plot is new, then a plot the status up to now
                if newPlot
                    AnimatedLines(i) = animatedline;
                    line = findobj(AnimatedLines(i));
                    set(gca,'XLim',[1 this.MaxIterations]);

                    line.addpoints(1:length(arr),arr);
                else
                    line = findobj(AnimatedLines(i));
                    %otherwise the last item is appended
                    line.addpoints(length(arr),arr(end));
                end
            end

            drawnow;
        end
    end
end

