%  AbstractTrainingEvent
%
%   This class defines an abstract events  triggered during training
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef (Abstract) AbstractTrainingEvent < handle & matlab.mixin.Copyable
    methods
        function this=AbstractTrainingEvent()
        end
        
        % Check if the stop criterion has been reached
        % Input:
        %   rbm: rbm that is trained
        %   t  : current epoch
        %
        % Output:
        %   stop: true if the stop criterion has been reached
        %
        stop = StopCriterion(this,rbm,t);
        
        % Show the results of the current mini-batch
        % Input:
        %   rbm: rbm that is trained
        %   t  : current epoch
        %   i  : current minibatch
        %   n  : total number of mini batches
        %
        MiniBatchResults(this,rbm,t,i,n);
        
        % Show the results of the current epoch
        % Input:
        %   rbm: rbm that is trained
        %   t  : current epoch
        %
        EpochResults(this,rbm,t);
        
        % Trigger an event when training is finished
        % Input
        %   rbm: rbm that is trained
        %
        TrainingEnded(this,rbm);
    end
end

