%  LibSVMProbeTrainingEvent
%
%   This class fit an SVM and classify using a validation set
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%

classdef  (Abstract) AbstractProbeTrainingEvent < Events.DefaultTrainingEvent
    properties
        X_train = []; %Train Data
        Y_train = []; %Train Data Labels/Target Variable
        X_val = []; %Validation Data
        Y_val = []; %Validation Data Labels/Target Variable
        
        Epochs   = [];
        TrainingResults = [];
        TrainingRMSE = [];
        ValidationResults = [];
        ValidationRMSE = [];
        
        %Compute the RMSE of the training set. This operation is also done
        %during the CD, at the end of each epoch when Debug>=1. However,
        %computing the RMSE each epoch is inefficient.
        RMSETrainingSet = false;
        
        YLimits = [0,100]; %%YLimits of the axes
        
        PlotResults = 0;
        
        %Probe when the mod(10,T) == 0, where T is the current epoch
        %If vector, probes at specific epochs
        ProbeAt = 10;
    end
    
    methods
        function this = AbstractProbeTrainingEvent(X_train,Y_train,X_val,Y_val)
            if (nargin>=1)
                this.X_train = X_train;
            end
            
            if (nargin>=1) && (nargin<=2)
                this.Y_train = Y_train;
            end
            
            if (nargin>=1) && (nargin<=3)
                this.X_val = X_val;
            end
            
            if (nargin>=1) && (nargin>3)
                this.Y_val = Y_val;
            end
        end
        
        function EpochResults(this,rbm,t)
            EpochResults@Events.DefaultTrainingEvent(this,rbm,t);
            
            if this.isProbing(t)
                this.PreProbeOperations(rbm,t);
            end
        end
        
        % Checks if at the current epoch a probing should be performed
        % Input
        %  t : Current Epoch
        %
        % Output:
        %   b: true if t is a valid epoch to probe, according to the ProbeAt
        %      property, otherwise it returns false
        %
        function b = isProbing(this,t)
            
            if (isscalar(this.ProbeAt))
                b = mod(t,this.ProbeAt)==0;
            else
                b=ismember(t,this.ProbeAt);
            end
        end
        
        Probe(this,rbm,t);
    end
    
    methods(Access=protected)
        function PreProbeOperations(this,rbm,t)
            
            if (~isempty(this.X_val))
                this.Epochs = [this.Epochs t];
                this.ValidationRMSE = [this.ValidationRMSE rbm.ReconstructionError(this.X_val)];
            end
            
            if (this.RMSETrainingSet && ~isempty(this.X_train))
                this.TrainingRMSE = [this.TrainingRMSE rbm.ReconstructionError(this.X_train)];
            end
            
            this.Probe(rbm,t);
        end
        
        function Plot(this,train,val,t)
            persistent Figure;
            persistent Training;
            persistent Validation;
            
            if (isempty(Figure) || ~ishandle(Figure))
                Figure = figure;
                Training = animatedline;
                Validation = animatedline;
                
                Training.Color   = 'blue';
                Validation.Color = 'red';
                
                legend('Training','Validation');
                
                xlabel('Epochs');
                ylabel('Performace');
                
                grid on;
                xlim([0,this.MaxIterations]);
                ylim(this.YLimits);
            end
            
            set(0, 'CurrentFigure', Figure)
            
            Training.addpoints(t,train);
            Validation.addpoints(t,val);
            
            
            drawnow;
        end
        
    end
end
