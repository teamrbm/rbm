%  SaveProbeTrainingEvent
%
%  This class allows to save the rbm state at certain epochs
%
%  Copyright (c) 2017 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef SaveProbeTrainingEvent < Events.AbstractProbeTrainingEvent
    properties
        OutputDir = './';
        Filename = 'RBM-<T>.mat';
    end
    
    methods
        function this = SaveProbeTrainingEvent(varargin)
            this = this@Events.AbstractProbeTrainingEvent(varargin{:});
        end
        function this = Probe(this,rbm,t)
            fname = fullfile(this.OutputDir,this.Filename);
            fname = strrep(fname,'<T>',sprintf('%d',t));
            
            if (this.Verbose)
                fprintf('\tSaving RBM Status...\n');
            end
            rbm.save(fname);
        end
    end
end