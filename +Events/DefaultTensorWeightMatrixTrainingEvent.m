%  DefaultTensorWeightMatrixTrainingEvent
%
%   This class defines defaults events for RBM training
%
%  Copyright (c) 2017 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%


classdef DefaultTensorWeightMatrixTrainingEvent < Events.DefaultTrainingEvent
    properties
        ShowDiff = [1 2];
    end
    
    methods
        function this=DefaultTensorWeightMatrixTrainingEvent()
        end
                
        function EpochResults(this,rbm,t)
            if this.PlotData
                this.ShowData(rbm,t);
            end
            
            if this.ShowWeightMatrix
                this.ShowW(rbm);
            end
            
            if (rbm.Debug > 0) && this.Verbose;
                T_mse = min (t,length(rbm.RMSE));
                T_et  = min (t,length(rbm.ElapsedTime));
                
                
                MiscFunctions.textprogressbar(sprintf(['\n Recon. Error: %f\tW Diff: %f\tSparsity: %f \t Eta: %f\tElapsed Time: %s\tMem: %.2fMB'],...
                    rbm.RMSE(T_mse),...
                    mean(abs(reshape(rbm.W(:,:,this.ShowDiff(1)) - rbm.W(:,:,this.ShowDiff(2)),1,[]))),...
                    mean(rbm.Sparsity),...
                    rbm.Eta.Value,...
                    datestr(datenum(0,0,0,0,0,rbm.ElapsedTime(T_et)),'HH:MM:SS'), ...
                    round(rbm.getMemorySize()./(1024.^2),2)));
            end
        end
        
    end
end

