% Explicit Rotation Invariant Gaussian Bernoulli Restricted Boltzmann Machine
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% References
% [1] M. V. Giuffrida and S. A. Tsaftaris, ?Rotation-Invariant Restricted 
%     Boltzmann Machine using shared gradient filters,? p. 8, Apr. 2016.
%
% See also: ExplicitRotInvRestrictedBoltzmannMachine, RestrictedBoltzmannMachine

classdef ExplicitRotInvGBRestrictedBoltzmannMachine < GaussianBernoulliRestrictedBoltzmannMachine & ExplicitRotInvRestrictedBoltzmannMachine
    
    methods
        function this = ExplicitRotInvGBRestrictedBoltzmannMachine(varargin)
            %Constructor for a Rotation Invariant RBM
            %   r_v: number of rows in the visible layer
            %   c_v: number of columns in the visible layer
            %   ch : color channels
            %   h: number of rows in the hidden layer
            
            if (numel(varargin)>2)
                v = cell(1,2);
                
                if (nargin==3)
                    v{1} = varargin{1}*varargin{2};
                    v{2} = varargin{3};
                else
                    v{1} = varargin{1}*varargin{2}*varargin{3};
                    v{2} = varargin{4};
                end
            else
                v = varargin;
            end
            
            this = this@GaussianBernoulliRestrictedBoltzmannMachine(v{:});
            
            this.RegularisationTerm = Regularisation.NoRegularisation(this);
            
            if (nargin>0)
                r_v = varargin{1};
                c_v = varargin{2};
                
                if (nargin<=3)
                    h = varargin{3};
                else
                    this.NumberChannels = varargin{3};
                    h = varargin{4};
                end
                
                this.initialise(r_v*c_v*this.NumberChannels,h);
                
                this.DataShape = [r_v,c_v];
                
                this.setupBins;
           end
        end
        
        function this = initialise(this,v,h)
            this = initialise@GaussianBernoulliRestrictedBoltzmannMachine(this,v,h);
            this.setupBins;
        end
        
        function H = probHgivenV(this,V,W)
            %Computes the p(h|v) probabilities
            % V: visibile layer
            % W: weight matrix (default this.W)
            % Output
            %   H: activation probabilities
            
            if (nargin<3)
                W = this.W;
            end
            
            if (strcmp(class(V),'Data.DefaultData') || isempty(V.Bins))
                H = probHgivenV@GaussianBernoulliRestrictedBoltzmannMachine(this,V,W);
            else
                H = zeros( size(V.data,1) , this.numberOfHiddenUnits, this.NumberOfBins);
                
                if (this.Cuda)
                    H = gpuArray(H);
                end
                
                for i=1:this.NumberOfBins
                    Weights = W(:,:,i);
                    
                    p = probHgivenV@GaussianBernoulliRestrictedBoltzmannMachine(this,V,Weights);
                    M = repmat(V.Bins(:,i),1,size(p.data,2));
                    
                    H(:,:,i) = p.data.*M;
                end
                
                H = sum(H,3);
                H = min(H,1); % gradients might be an epsilon greater than 1
                
                H_raw = H;
                H = V.copy();
                H.X = H_raw;
            end
        end
        
         function V = probVgivenH(this,H)
            %Computes the p(v|h) probabilities
            % V: visibile layer
            % Output
            %   H: activation probabilities
            
            V = zeros( size(H.data,1) , this.numberOfVisibleUnits,this.NumberOfBins);
            
            if (this.Cuda)
                V = gpuArray(V);
            end
            
            for i=1:this.NumberOfBins
                Weights = this.W(:,:,i);

                     
                p = probVgivenH@GaussianBernoulliRestrictedBoltzmannMachine(this,H,Weights);
                
                M = repmat(H.Bins(:,i),1,size(p.data,2));
                
                V(:,:,i) = p.data.*M;
            end
            
            V = sum(V,3);
            
            V_raw = V;
            V = H.copy();
            V.X = V_raw;
         end
        
         function this = copyToDevice(varargin)
             this = copyToDevice@GaussianBernoulliRestrictedBoltzmannMachine(varargin{:});
         end
         
         function this = gatherData(this)
             this = gatherData@GaussianBernoulliRestrictedBoltzmannMachine(this);
         end
        
    end
    
    methods(Access=protected)
        function data = dataToDump(this)
            data1 = dataToDump@GaussianBernoulliRestrictedBoltzmannMachine(this);
            data2 = dataToDump@ExplicitRotInvRestrictedBoltzmannMachine(this);
            
            [~,~,idx] = intersect(data1(:,1),data2(:,1));
            data = cat(1,data1,data2(setdiff(1:size(data2,1),idx),:));
        end
        
        function nabla = computeGradients(this,v0,h0,v1,h1)
            nData = size(v0.data,1);
            
            sigma2 = repmat(this.getVariance(),nData,1);
            
            v0.X = v0.data./sigma2;
            v1.X = v1.data./sigma2;
            
            nabla = computeGradients@RestrictedBoltzmannMachine(this,v0,h0,v1,h1);
            nabla = this.computeERIGradients(nabla,v0,h0,v1,h1);                                                
            
            if (this.updateSigma)
                data_visible_term  = ((v0.data - repmat(this.c,nData,1)).^2);
                model_visible_term = ((v1.data - repmat(this.c,nData,1)).^2);

                data_mixed_term  = zeros(nData,this.numberOfVisibleUnits, this.NumberOfBins);
                model_mixed_term = zeros(nData,this.numberOfVisibleUnits, this.NumberOfBins);

                if (this.Cuda)
                    data_mixed_term = gpuArray(data_mixed_term);
                    model_mixed_term = gpuArray(model_mixed_term);
                end

                % TODO: check if it is correct
                for i = 1:this.NumberOfBins
                    M = repmat(v0.Bins(:,i),1,size(v0.data,2));
                    data_mixed_term(:,:,i) =  M.*v0.data .* (h0.data*this.W(:,:,i)');
                    model_mixed_term(:,:,i) = M.*v1.data .* (h1.data*this.W(:,:,i)');
                end

                data_mixed_term = sum(data_mixed_term,3);
                model_mixed_term = sum(model_mixed_term,3);

                nabla.z = sum(repmat(exp(-this.z),nData,1) .* (( data_visible_term - data_mixed_term ) - (model_visible_term - model_mixed_term) ));
            else
                nabla.z = zeros(size(this.z));
            end
            
        end
    end
    
    methods(Static)
        function E = freeEnergy(varargin)
            if ((nargin>1) && isa(varargin{2},'ExplicitRotInvGBRestrictedBoltzmannMachine'))
                [X,r] = deal(varargin{:});
                E = ExplicitRotInvGBRestrictedBoltzmannMachine.freeEnergy(X,r.W,r.c,r.b,r.z);
            else
                
                [X,W,c,b,z] = deal(varargin{:});                  
                
                nBins = size(X.Bins,2);
                
                Es = zeros(size(X.data,1),nBins);
                
                for i=1:nBins
                    Weights = W(:,:,i);  
                    
                    % Read freeEnergy function in
                    % ExplicitRotInvRestrictedBoltzmannMachine for further
                    % details
                    
                    D =     X.data .* repmat(X.Bins(:,i),1,size(X.data,2));
                    hbias = X.Bins(:,i) * b;
                    
                    nData = size(D,1);
                    sigma2 = exp(repmat(z,nData,1));

                    visible_term = sum((D - repmat(c,nData,1)).^2./(2*sigma2),2);

                    D = D ./ sigma2;

                    exp_term = hbias + D*Weights;
                    exp_term_p = max(exp_term,0);

                    hidden_term = sum( log(exp(-exp_term_p) + exp(exp_term - exp_term_p)) + exp_term_p ,2);

                    Es(:,i) = gather(visible_term - hidden_term);
                    
                    %Es(:,i) =  GaussianBernoulliRestrictedBoltzmannMachine.freeEnergy(D,Weights,c,hbias);
                end

                E = sum(Es,2);
            end
        end
    end
end
