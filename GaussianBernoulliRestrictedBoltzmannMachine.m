% Gaussian-Bernoulli Restricted Boltzmann Machine
%
%  Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% Coded inspired from:
%   - http://www.kyunghyuncho.me/home/code
%
% References
%  [1] K. Cho, A. Ilin, and T. Raiko, "Improved learning of Gaussian- Bernoulli restricted Boltzmann machines" 
%      in Proceedings of the 20th International Conference on Artificial Neural Networks (ICANN 2010), 2011.
%
% See also: RestrictedBoltzmannMachine, AbstractMomentum, ConstantMomentum, PiecewiseMomentum


classdef GaussianBernoulliRestrictedBoltzmannMachine < RestrictedBoltzmannMachine
    properties
        z; % Log of the sigma (variance of the Gaussian distribution)
        
        %Tracked variables
        zs = []; %tracking how the variances are varying across time
        
        %Setups
        updateSigma = 1;
        SigmaInitialiser = SigmaInitialiser.DefaultSigmaInitialiser;
        
        %%
        EtaSigmaMultiplier = 1; %Number that it is multiplied to the Learning Rate only when sigmas are updated
        SigmaBounds = struct('Upper',Inf,'Lower',0);
    end
    
    methods
        function this = GaussianBernoulliRestrictedBoltzmannMachine(varargin)
            this@RestrictedBoltzmannMachine(varargin{:});
            
            if (nargin>0)
                this.LastGradients.dZ = 0;
            end
        end
                
        function sigma2 = getVariance(this,N)
            %Returns the variance of each visible unit within the GBRBM model
            % Input:
            %   N: replicates the variance N times (optional)
            %
            % Output:
            %   sigma: 1xV (or NxV if N is specified) vector with sigma2, where
            %          V is the number of visible units
            %
            
            if (nargin==1)
                N=1;
            end
            
            sigma2 = exp(this.z);
            
            if (N>1)
                sigma2 = repmat(sigma2,N,1);
            end
        end
        
        function sigma = getStandardDeviation(this,varargin)
            %Returns the standard deviation of each visible unit within the GBRBM model
            %
            %SEE ALSO: getVariance
            
            sigma = sqrt(this.getVariance(varargin{:}));
        end
        
        
        function H = probHgivenV(this,V,Weights)            
            if (nargin<3)
                Weights = this.W;
            end
            
            v = V.data ./ repmat(this.getVariance(),size(V.data,1),1);
            
            p = RestrictedBoltzmannMachine.sigmoid( bsxfun(@plus,v * Weights,this.b));
            
            %copy the structure
            H = V.copy();
            H.X = p;
        end
        
        
        
        function V = probVgivenH(this,H,Weights)
            %Computes the p(v|h) probabilities
            % h = invisible layer
            % W: weight matrix (default this.W)
            %
            % Output
            %   p: activation probabilities
            
            if (nargin<3)
                Weights = this.W;
            end
            
            p = bsxfun(@plus, H.data * Weights',this.c);
            
            %copy the structure
            V = H.copy();
            V.X = p;
        end
        
        function V2 = drawV(this,V)
            V2 = V.copy();
            sigma = this.getStandardDeviation();
            V2.X = normrnd(V.data,repmat(sigma,size(V.data,1),1));
        end  
        
        
        function D = copyToDevice(this,D)
            copyToDevice@RestrictedBoltzmannMachine(this,D);
            
            if (this.Cuda)
               this.z = gpuArray(this.z);
            end
        end
        
        function this = gatherData(this)
            gatherData@RestrictedBoltzmannMachine(this);
            
            if (this.Cuda)
                this.z = gather(this.z);
            end
        end
        
        
    end
    
     methods(Access=protected)
        function this = postprocessedData(this,D)
	    if (~isempty(this.SigmaInitialiser))
            	this.z = this.SigmaInitialiser.initialise(D.data);
	    end
        end
        

        
        function nabla = computeGradients(this,v0,h0,v1,h1)      
            nData = size(v0.data,1);
            
            sigma2 = repmat(this.getVariance(),nData,1);
            
            v0.X = v0.data./sigma2;
            v1.X = v1.data./sigma2;
            
            nabla = computeGradients@RestrictedBoltzmannMachine(this,v0,h0,v1,h1);
                                                            
            if (this.updateSigma)
                data_visible_term  = ((v0.data - repmat(this.c,nData,1)).^2);
                model_visible_term = ((v1.data - repmat(this.c,nData,1)).^2);

                data_mixed_term  =  v0.data .* (h0.data*this.W');
                model_mixed_term =  v1.data .* (h1.data*this.W');

                nabla.z = sum(repmat(exp(-this.z),nData,1) .* (( data_visible_term - data_mixed_term ) - (model_visible_term - model_mixed_term) ));
            else
                nabla.z = zeros(size(this.z));
            end
            
            nabla.z = nabla.z .* this.EtaSigmaMultiplier;
            
        end
        
        function varargout = updateTheta(this,nabla,fakeUpdate,addLastGradients)
            if (this.updateSigma && isfield(nabla,'z'))
                newZ = this.z + nabla.z;
            else
                newZ = this.z;
            end
            
            newZ = min( this.z, log(this.SigmaBounds.Upper));
            newZ = max( this.z, log(this.SigmaBounds.Lower));
            
            if (nargin<3)
                fakeUpdate=false;
            end
            
            if (nargin<4)
                addLastGradients=false;
            end
            
            r = updateTheta@RestrictedBoltzmannMachine(this,nabla,fakeUpdate,addLastGradients);
            
            if (~fakeUpdate)
                this.z = newZ;

                if ((this.Debug > 1) && ~this.Cuda)
                    this.appendTo('zs',this.z);
                end
            else
                r.z = newZ;
                varargout{1} = r;
            end
        end
        
        function this = initialiseTrackedVariables(this)
            initialiseTrackedVariables@RestrictedBoltzmannMachine(this);
            this.zs = zeros(this.LoggingLength,numel(this.c));
        end
        
        function data = dataToDump(this)
            data = dataToDump@RestrictedBoltzmannMachine(this);
            
            keys   = {'z','updateSigma'}';
            values = MiscFunctions.readProperties(this,keys);
            
            data = cat(1,data,cat(2,keys,values));
        end
     end
     
     methods(Static)
         function E = freeEnergy(varargin)
             if ((nargin>1) && isa(varargin{2},'GaussianBernoulliRestrictedBoltzmannMachine'))
                [X,r] = deal(varargin{:});
                E = GaussianBernoulliRestrictedBoltzmannMachine.freeEnergy(X,r.W,r.c,r.b,r.z);
             else
                [X,W,c,b,z] = deal(varargin{:});
                
                if (isobject(X))
                    X = X.data;
                end
                
                nData = size(X,1);
                sigma2 = exp(repmat(z,nData,1));

                visible_term = sum((X - repmat(c,nData,1)).^2./(2*sigma2),2);

                X = X ./ sigma2;

                exp_term = repmat(b,nData,1) + X*W;
                exp_term_p = max(exp_term,0);

                hidden_term = sum( log(exp(-exp_term_p) + exp(exp_term - exp_term_p)) + exp_term_p ,2);

                E = gather(visible_term - hidden_term);
             end
        end
    end
end
