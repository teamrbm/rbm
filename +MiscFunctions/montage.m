function b=montage(I,sz)
    if (ndims(I)<=3)
        [r,c,n] = size(I);
        ch=1;
        I = permute(I,[1 2 4 3]);
    elseif ndims(I)==4
        [r,c,ch,n] = size(I);
    else
        error('Invalid Image Size');
    end
    
    if ((nargin<2)||(isempty(sz)))
        sz = [1 1] .* ceil(sqrt(size(I,4)));
    elseif isscalar(sz)
        sz = [1 1] .* ceil(sqrt(sz));
    end
   
    
    b = double(zeros(sz(1)*r,sz(2)*c,ch));
    
    for k=1:min(n,prod(sz))
        i = floor((k-1)/sz(1));
        j = mod(k-1,sz(2));
        
        a = I(:,:,:,k);
        
        m = min(a(:)); M = max(a(:));
        a = (a-m)./(M-m);
        
        deltaX = i*r; deltaY = j*c;
        b(deltaX+1:deltaX+r,deltaY+1:deltaY+c,:) = a;
    end
    
end