% Read Properties of a struct or object from multiple field (also nested)
%
% Copyright (c) 2017 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% SEE ALSO: writeProperties

function [ values ] = readProperties( obj,prop )
    values=cell(size(prop));
    
    for i=1:numel(prop)
        eval(sprintf('values{i} = obj.%s;',prop{i}));
    end
end

