% Abstract Restricted Boltzmann Machine
%
% Copyright (c) 2016 - Valerio Giuffrida <valerio.giuffrida@imtlucca.it>
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; either version 3
% of the License, or any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% Coded inspired from:
%   - http://www.mathworks.com/matlabcentral/fileexchange/42853-deep-neural-network
%   - http://www.kyunghyuncho.me/home/code
%
% References
%  [1] Hinton, G. (2010)."A Practical Guide to Training Restricted Boltzmann Machines.
%      Computer, 9(3), 1. doi:10.1007/978-3-642-35289-8_32
%
% See also:
% RestrictedBoltzmannMachine,GaussianBernoulliRestrictedBoltzmannMachine
%
classdef (Abstract) AbstractRestrictedBoltzmannMachine < matlab.mixin.SetGet & handle
    
    properties        
        Eta = []; %Eta Update rule. Default is ContanstLearningRate
        Momentum = Momentum.ConstantMomentum;   %Update rule momentum. Default ConstantMomentum.
        WeightCost= 0.00001; %Weight decay. Default 0.00001.
               
        DrawSamples = struct('V',false,'H',true);
        NumberGibbsSamplingSteps = 1; %Number of Gibbs Sampling Iteration. Default 1
        
        RegularisationTerm = Regularisation.NoRegularisation; %Additional Regularisation Term (e.g. sparisity). Default none
        Persistent = false; %Persistent Gibbs Sampling
        MiniBatchSize = 100;      %Number of data being used for each iteration. Default 100.
        
        %RBM setups
        Debug = 0;                %Level of debugging. Default 0
        Cuda = false;   %whether or not use CUDA
        GPUDevice = 1;  %GPU Device to work with (only if CUDA is enabled)
                
        %Optimisation tracking
        %The following properties refers to debug level 1
        
        RMSE = []; %Tracks the reconstruction error
        Energies = []; %Tracks the free energies
        ElapsedTime = []; %Tracks time occured for each epoch
        Sparsity = []; %Tracks the sparsity factor for each batch
        LastReconstructionData = struct('X',[],'Xr',[]);
        LoggingLength = 100; %Number of epochs to keep track of values to track (Etas,RMSE,...)
        
        %Handles
        TrainingEventListener = Events.DefaultTrainingEvent;
        
        Profiler = false;
        
    end
    
    properties(SetAccess=protected,GetAccess=public)
        %Status of the RBM
        % 1: Initialised
        % 2: Trainining
        % 3: Trained
        % Useful for resuming computations
        
        Status = 'initialised'; %Status of the RBM
        LastGradients = []; %Gradient computed at previous batches. Used to apply momentum
        Epoch = 0; %Iteration number
    end
    
    
    properties(Access=protected)        
        nBatch = 1; %Number of current optimised batch
        totBatches = 0; %Total number of batches
        DataRandomVector = []; %Random permutation of data
        PersistentState = []; %State of the previous Gibbs Sampling to be used in the Persistent Gibbs Sampling
    end
    
    
    methods
        
        function this = AbstractRestrictedBoltzmannMachine()
            this.Eta = LearningRate.ConstantLearningRate(this);
        end
        
        
       
                
        this=Train(this,varargin);
        nabla = this.applyMomentum(nabla,learningRate,momentum);
        varargout = applyGradients(this,nabla);
                      
        
        function n = getNumberOfMiniBatches(this,X)
            %Computes the number of minibatches
            %   X: input data
            % Output
            %   n: number of minibatches
            
            n = ceil( size(X,1) / this.MiniBatchSize);
        end
        
        
        function Xd = getBatch(this,D,t)
            %Get the corresponding chunk of data from the specified minibatch
            %   X: input data
            %   t: number of the desider minibatch
            % Output
            %   Xd: extracted chunk of data
            
            idx = this.getBatchIndices(t);
            Xd = D(idx);
            
                        
            if (this.Cuda && ~isa(Xd.X,'gpuArray'))
                %move data into the device
                %cast into double is important because matlab does not
                %support matrix multiplication between integer cuda arrays
                
                Xd.X = gpuArray(cast(Xd.X,'double'));
            end
        end
        
        
        function idx = getBatchIndices(this,t)
            %Returns the indices of the samples belonging to a specific batch.
            %Since samples are randomly selected, this function returns a set of
            %random indices that address a set of samples within a batch.
            %The set of random indices is always the same, if t is the same.
            %The random selection is performed at the beginning of the train
            %procedure.
            %
            %   t: number of batch
            % Output
            %   idx: set of indices belonging to the t-th batch
            
            nBatches = this.totBatches;
            
            %the number of batches is wrapped. if t goes beyond T (or below 0),
            %here I wrap it in order to start from the beginning (or end)
            
            if (t>nBatches)
                t = mod(t-1,nBatches)+1;
            elseif (t<1)
                t = mod(t,nBatches);
            end
            
            if (t==0)
                t = nBatches;
            end
            
            from = this.MiniBatchSize * (t-1) + 1;
            to   = min(this.MiniBatchSize * t,numel(this.DataRandomVector));
            
            idx = this.DataRandomVector(from:to);
        end
        
        
        function [sz,peak] = getMemorySize(this)
            % Returns the number of bytes occupied by the RBM
            %
            % Output:
            %   sz: size in bytes of the RBM
            %
            
            t = profile('status');
            
            if nargout>1
                peak = NaN;
            end
            
            if (strcmp(t.ProfilerStatus,'off'))
                props = properties(this);
                sz = 0;

                for i=1:numel(props)
                    currentProperty = this.(props{i});
                    s = whos('currentProperty');
                    sz = sz + s.bytes;
                end
            else
                p = profile('info');
                str = {p.FunctionTable.FunctionName};
                f = cellfun(@(x)(~isempty(x)),cellfun(@(x)(strfind(x,'.Train')),str,'UniformOutput',false));
                idx = find(f,1);
                sz = p.FunctionTable(idx).TotalMemAllocated - p.FunctionTable(idx).TotalMemFreed;
                
                if (nargout>1)
                    peak = p.FunctionTable(idx).PeakMem;
                end
                
                profile resume
            end
        end
        

        
        H = extractFeatures(this,X);
        
        
        function rmse=ReconstructionError(this,X)
            if (~X.Preprocessed)
                X.preprocess;
            end
            
            H  = this.probHgivenV(X);           
            Xr = this.probVgivenH(H);
            
            rmse = mean(sqrt(sum((X - Xr).^2,2)));
        end
        
        function this = gatherData(this)
        end
        
        function save(model,filename)
            % Save the current machine in the disk
            % Input:
            %   filename: .MAT filename where you want to save the current
            %              machine
            
            model.gatherData();
            save(filename,'model','-v7.3');
        end
        
        function dump(this,filename)
            % Save the essential data of an RBM (e.g. W,b,c)
            %
            % SEE ALSO: save
            
            model = this.dataToDump();
            save(filename,'model');
        end
        
       
       
        function  this = loadDump(this,varargin)
            if (ischar(varargin{1}))
                load(varargin{1});
                this.loadDump(model);
            elseif (iscell(varargin{1}))
                this = MiscFunctions.writeProperties(this,varargin{1});
            end
        end
        
        function this = set.Profiler(this,b)
            this.Profiler = b;
            
            profile off
            
            if (b)                
                profile -memory on
            end
            
        end
        
        function r = isGPUActive(this)
            r = this.cudaCheck;
        end
    end
    
    methods(Access=protected)
        
        function data = dataToDump(this)
            keys   = {'Eta.Value','WeightCost','DrawSamples','Epoch','MiniBatchSize'}';
            values = MiscFunctions.readProperties(this,keys);
            
            for i=1:numel(values)
                values{i} = gather(values{i});
            end
            
            data = cat(2,keys,values);
        end
                
        function r = cudaCheck(this)
            %Sanity check: verify if cuda is available and/or can be used
            r=false;
            if (this.Cuda)
                try
                    %no cuda device, forcing learning by cpu
                    gpus = gpuDevice;
                    if (isempty(gpus) && nargout==0)
                        throw(MException('',''));
                    else 
                        r=true;
                    end
                catch E
                    %no cuda device found
                    this.Cuda = false;
                    if (nargout==0)
                        warning('No CUDA device found. Training in CPU');
                    end
                end
                
            else
                try
                    %if you've got cuda, Use it.
                    gpus = gpuDevice;
                    if (~isempty(gpus))
                        warning('This machine has CUDA capabilities. Try to set the parameter ''Cuda'' to true to make the code faster');
                    end
                catch E
                    %you have not selected cuda and you don't have supported hw.
                    %Skip it.
                end
            end
        end
        
        function appendTo(this,field,data,t)
            %Append a piece of data to a matrix
            %
            % field: string indentifying the field to update
            % data : data to append to field
            
            if (~exist('t','var'))
                t = this.Epoch;
            end
            
            v = this.(field);
            
            if (isa(data,'gpuArray'))
                data = gather(data);
            end
            
            if (isvector(v))
                sz = numel(v);
                %check if the data is a scalar
                if (isscalar(data))
                    %check if v can take data
                    if (sz<t)
                        t = sz;
                        %if it cannot, v is shifted and earlier data are
                        %discarded
                        if (isrow(v))
                            v = circshift(v,[0 -1]);
                        else
                            v = circshift(v,[-1 0]);
                        end
                    end
                    %append data
                    v(t) = data;
                else
                    error('Only scalars can be appended to a matrix');
                end
            elseif (ismatrix(v))
                [row, cols] = size(v);
                
                if (any([row,cols]==numel(data)))
                    if (isvector(data))
                        %in this case, rows are appendend
                        if (cols == numel(data))
                            sz = size(v,1);
                            if (sz<t)
                                v = circshift(v,[-1,0]);
                                t=sz;
                            end
                            
                            v(t,:) = data;
                        else %in this case columns are appended
                            sz = size(v,2);
                            if (sz<t)
                                v = circshift(v,[0,-1]);
                                t=sz;
                            end
                            
                            v(:,t) = data;
                        end
                    else
                        error('Only vector can be appended to a matrix');
                    end
                else
                    error('Data size does not match with the field size');
                end
            elseif (ndims(v)==3)
                [row,cols,sz] = size(v);
                
                if (all([row,cols] == size(data)))
                    if (sz<t)
                        t = sz;
                        v = circshift(v,[0,0,-1]);
                    end
                    
                    v(:,:,t) = data;
                else
                    error('Data size does not match to a n-m-t matrix');
                end
                
            end
            
            %update the result
            this.(field)=v;
        end
        
        nabla = computeGradients(~,v0_drawn,h0_prob,v1_drawn,h1_prob);
         
    end
    
    methods(Static)
        E = freeEnergy(varargin);
        
        function x = sigmoid(x)
            %Computes the sigmoid function
            
            x = 1./(1 + exp(-x));
        end
        
    end
end
